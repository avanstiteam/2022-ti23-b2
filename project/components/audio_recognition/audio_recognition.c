/**
 * Example ESP-ADF application using a Goertzel filter
 *
 * This application is an adaptation of the
 * Voice activity detection (VAD) example application in the
 * ESP-ADF framework by Espressif
 * https://github.com/espressif/esp-adf/tree/master/examples/speech_recognition/vad
 *
 * Goertzel algoritm initially implemented by P.S.M. Goossens,
 * adapted by Hans van der Linden, editted by Jesse Krijgsman & Stijn van Tilburg
 *
 * Avans Hogeschool, Opleiding Technische Informatica
 */

#include <math.h>

#include "freertos/FreeRTOS.h"

#include "esp_err.h"
#include "esp_log.h"
#include "board.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "filter_resample.h"
#include "Input_Codes.h"
#include "Input_Send.h"
#include "goertzel_filter.h"

static const char *TAG = "AUDIO_RECOGNITION";

#define GOERTZEL_SAMPLE_RATE_HZ 8000 // Sample rate in [Hz]
#define GOERTZEL_FRAME_LENGTH_MS 100 // Block length in [ms]

#define GOERTZEL_BUFFER_LENGTH (GOERTZEL_FRAME_LENGTH_MS * GOERTZEL_SAMPLE_RATE_HZ / 1000) // Buffer length in samples

#define GOERTZEL_DETECTION_THRESHOLD 50.0f // Detect a tone when log magnitude is above this value

#define AUDIO_SAMPLE_RATE 48000 // Audio capture sample rate [Hz]

#define AUDIO_TRIGGER_DELAY_MS 500 // Trigger delay in milliseconds

void deinit_tone_detection();
void init_tone_detection();
void tone_detection_task(void *parameters);

int32_t lastTrigger = 0;
int16_t *rawBuffer;

/**
 * @brief Freq_Command gives stores the frequency and the command that needs te be called
 *
 */
struct Freq_Command
{
    int freq;
    int inputCommand;
};

/** 
 *  Storing all the frequency's bounded to the buttons they're responsible for
 */
struct Freq_Command freqCommands[] = {
    {650, MENU_KEY_NULL},   /**adding a frequency below MENU_KEY_RIGHT improves error detection due to the
                             multiple frequency detection system in the goertzel filter module **/
    {699, MENU_KEY_RIGHT},  // Piano E5
    {784, MENU_KEY_LEFT},   // Piano F5
    {880, MENU_KEY_SELECT}, // Piano G5
    {988, MENU_KEY_BACK},  // Piano A5
    {1050, MENU_KEY_NULL}   /**adding a frequency above MENU_KEY_BACK improves error detection due to the
                               multiple frequency detection system in the goertzel filter module **/
};

#define GOERTZEL_NR_FREQS ((sizeof freqCommands) / (sizeof freqCommands[0])) // Number of value's in the array

    audio_pipeline_handle_t toneDetectionPipeline;

    audio_element_handle_t i2sStreamReader;
    audio_element_handle_t resampleFilter;
    audio_element_handle_t rawReader;

    goertzel_filter_cfg_t filtersCfg[GOERTZEL_NR_FREQS];
    goertzel_filter_data_t filtersData[GOERTZEL_NR_FREQS];

/**
 * creates an i2s stream in the form of an audio_element_handle_t
 */
static audio_element_handle_t create_i2s_stream(int sample_rate, audio_stream_type_t type)
{
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = type;
    i2s_cfg.i2s_config.sample_rate = sample_rate;
    audio_element_handle_t i2s_stream = i2s_stream_init(&i2s_cfg);
    return i2s_stream;
}

/**
 * creates a resample filter in the form of an audio_element_handle_t
 */
static audio_element_handle_t create_resample_filter(int source_rate, int source_channels, int dest_rate, int dest_channels)
{
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = source_rate;
    rsp_cfg.src_ch = source_channels;
    rsp_cfg.dest_rate = dest_rate;
    rsp_cfg.dest_ch = dest_channels;
    audio_element_handle_t filter = rsp_filter_init(&rsp_cfg);
    return filter;
}

/**
 * creates a raw stream in the form of an audio_element_handle_t
 */
static audio_element_handle_t create_raw_stream()
{
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    audio_element_handle_t rawReader = raw_stream_init(&raw_cfg);
    return rawReader;
}

/**
 * creates the pipeline
 */
static audio_pipeline_handle_t create_pipeline()
{
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    audio_pipeline_handle_t pipeline = audio_pipeline_init(&pipeline_cfg);
    return pipeline;
}

/**
 * Calls the right methods for the commands connected to the found frequency
 */
static void detect_freq(struct Freq_Command freqCommand, float magnitude)
{
    int32_t currentTime = xTaskGetTickCount();

    if ((currentTime - lastTrigger) * 10 > AUDIO_TRIGGER_DELAY_MS)
    {
        ESP_LOGI(TAG, "Detection at frequency %d Hz (magnitude %.2f)", freqCommand.freq, magnitude);

        if (freqCommand.inputCommand != MENU_KEY_NULL) Input_SendCommand(freqCommand.inputCommand);
    }
}

/**
 * malloc's and initializes all audio detection components.
 */
void init_tone_detection() {
        ESP_LOGI(TAG, "Number of Goertzel detection filters is %d", GOERTZEL_NR_FREQS);

    ESP_LOGI(TAG, "Create raw sample buffer");
    rawBuffer = (int16_t *)malloc((GOERTZEL_BUFFER_LENGTH * sizeof(int16_t)));
    if (rawBuffer == NULL)
    {
        ESP_LOGE(TAG, "Memory allocation for raw sample buffer failed");
        return;
    }

    ESP_LOGI(TAG, "Setup Goertzel detection filters");
    for (int f = 0; f < GOERTZEL_NR_FREQS; f++)
    {
        filtersCfg[f].sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
        filtersCfg[f].target_freq = freqCommands[f].freq;
        filtersCfg[f].buffer_length = GOERTZEL_BUFFER_LENGTH;
    }

    GOERTZEL_FILTER_setup(filtersData, filtersCfg, GOERTZEL_NR_FREQS);
    ESP_LOGI(TAG, "Create pipeline");
    toneDetectionPipeline = create_pipeline();

    ESP_LOGI(TAG, "Create audio elements for pipeline");
    i2sStreamReader = create_i2s_stream(AUDIO_SAMPLE_RATE, AUDIO_STREAM_READER);
    resampleFilter = create_resample_filter(AUDIO_SAMPLE_RATE, 2, GOERTZEL_SAMPLE_RATE_HZ, 1);
    rawReader = create_raw_stream();

    ESP_LOGI(TAG, "Register audio elements to pipeline");
    audio_pipeline_register(toneDetectionPipeline, i2sStreamReader, "i2s");
    audio_pipeline_register(toneDetectionPipeline, resampleFilter, "rsp_filter");
    audio_pipeline_register(toneDetectionPipeline, rawReader, "raw");

    ESP_LOGI(TAG, "Link audio elements together to make pipeline ready");
    const char *link_tag[3] = {"i2s", "rsp_filter", "raw"};
    audio_pipeline_link(toneDetectionPipeline, &link_tag[0], 3);

    ESP_LOGI(TAG, "Start pipeline");
    audio_pipeline_run(toneDetectionPipeline);

    xTaskCreate(tone_detection_task, "Tone detection task", 4096, NULL, 1, NULL);
}

/**
 * free's and deinit's all audio detection component.
 */
void deinit_tone_detection() {
        // Clean up (if we somehow leave the while loop, that is...)
    ESP_LOGI(TAG, "Deallocate raw sample buffer memory");
    free(rawBuffer);

    audio_pipeline_stop(toneDetectionPipeline);
    audio_pipeline_wait_for_stop(toneDetectionPipeline);
    audio_pipeline_terminate(toneDetectionPipeline);

    audio_pipeline_unregister(toneDetectionPipeline, i2sStreamReader);
    audio_pipeline_unregister(toneDetectionPipeline, resampleFilter);
    audio_pipeline_unregister(toneDetectionPipeline, rawReader);

    audio_pipeline_deinit(toneDetectionPipeline);

    audio_element_deinit(i2sStreamReader);
    audio_element_deinit(resampleFilter);
    audio_element_deinit(rawReader);
}

/**
 * The task responsible for detecting audio input.
 */
void tone_detection_task(void *parameters)
{
    while (1)
    {

        raw_stream_read(rawReader, (char *)rawBuffer, GOERTZEL_BUFFER_LENGTH * sizeof(int16_t));
        for (int f = 0; f < GOERTZEL_NR_FREQS; f++)
        {
            //processes the data for every filter
            ESP_ERROR_CHECK(GOERTZEL_FILTER_process(filtersData, GOERTZEL_NR_FREQS, rawBuffer, GOERTZEL_BUFFER_LENGTH));
        }
        
        //Checks if a frequency was found and returns that number. If multiple were found returns -1.
        int detectedFrequency = GOERTZEL_FILTER_check_target_frequencies(filtersData, GOERTZEL_NR_FREQS, GOERTZEL_DETECTION_THRESHOLD);
        //-1 is the error code, the first and last frequency's are only there to improve error detection
        if (detectedFrequency > 0 && detectedFrequency < GOERTZEL_NR_FREQS - 1)
        {
            ESP_LOGI(TAG, "Detection at frequency %d Hz (magnitude %.2f)", filtersCfg[detectedFrequency].target_freq, filtersData[detectedFrequency].magnitude);
            detect_freq(freqCommands[detectedFrequency],  filtersData[detectedFrequency].magnitude);
        }
    
        //resets all of the data.
        GOERTZEL_FILTER_clear(filtersData, GOERTZEL_NR_FREQS);
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }

    deinit_tone_detection();
}

/**
 * @brief Starts the detection of audio for audio control
 */
void AUDIO_DETECTION_Start(void)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);

    // Perform tone detection task
    init_tone_detection();
}