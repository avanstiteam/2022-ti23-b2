
#ifndef AUDIO_RECOGNITION
#define AUDIO_RECOGNITION

/**
 *  Starts the detection of audio for audio control
 */
void AUDIO_DETECTION_Start(void);
#endif
