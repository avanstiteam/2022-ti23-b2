#include "AbstractMenu.h"
#include "MenuHelper.h"
#include <stdbool.h>
#include <assert.h>

// Methods for handling menu actions
static void onExit();
static void onEntry();
static void switchScreen(int index);

bool AM_initialised = false;

// Current menu screen
MENU_SCREEN_ITEM *currentScreen;

/*
 * Given a starts screen, starts the menu
 */
void abstract_menu_startMenu(MENU_SCREEN_ITEM *startMenu) {
    // Set to the current screen
    currentScreen = startMenu;

    // Show the first menu
    menu_helper_show_menu(startMenu);

    // Calling the onEntry
    currentScreen->fpOnEntry();
    AM_initialised = true;
}

/*
 * Given a button int
 * The input will be handled by the screen
 */
void abstract_menu_handleKeyInput(int key) {
    // Check for all the valid key's
    assert(key >= 0 && key <= NUMBER_OF_KEYS);
    if(AM_initialised){
    // Checking for an OnClick
    if (NULL != currentScreen->fpOnKey[key]) {
		(currentScreen->fpOnKey[key])();
	}
        
    // Checking if we need to switch
    if (NULL != currentScreen->nextScreen[key]) {
		// Switching screen
        switchScreen(key);
	}
    }
}

/*
 * Given a screen to switch, the new menu will be shown
 */
void abstract_menu_switchScreen(MENU_SCREEN_ITEM *screen) {
    // Calling the onExit
    onExit();

    // Switching menu
    currentScreen = screen;   
    menu_helper_show_menu(currentScreen);

    // Calling the onEntry
    onEntry();
}

/*
 * Switches the screen to the given index of the current menu
 */ 
static void switchScreen(int index) {
    abstract_menu_switchScreen(currentScreen->nextScreen[index]);
}

/*
 * Handles the onExit of the current menu
 */
static void onExit() {
    if (NULL != currentScreen->fpOnExit) {
        currentScreen->fpOnExit();  
    }
}

/*
 * Handles the onEntry of the current menu
 */
static void onEntry() {
    if (NULL != currentScreen->fpOnEntry) {
        currentScreen->fpOnEntry();  
    }
}