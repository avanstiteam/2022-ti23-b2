#ifndef MENU_HELPER_H
#define MENU_HELPER_H

#include "AbstractMenu.h"

/*
* Shows the menu to the LCD screen
* Screen: MENU_SCREEN_ITEM, the screen to be drawn
*/
void menu_helper_show_menu(MENU_SCREEN_ITEM *screen);

#endif