#ifndef ABSTRACT_MENU_H
#define ABSTRACT_MENU_H

#include <stdio.h>
#include <stdbool.h>
#include "LCDUtil.h"

#define MAX_TEXT_LINES 6 // 6 Lines possible: Main_centre, Sub_centre, Top_left, Top_right, Bottom_left, Bottom_right
#define NUMBER_OF_KEYS 4 // 4 keys: Left, Right, click, Back

extern bool AM_initialised;
/*
 * MENU_SCREEN_ITEM defines one screen contained in the 
 * menu
 */
typedef struct menu_item MENU_SCREEN_ITEM;
 
// Defenition of the menu screen 
struct menu_item
{

    char* text[MAX_TEXT_LINES];                     // Array filled with the textlines for the menu
    MENU_SCREEN_ITEM *nextScreen[NUMBER_OF_KEYS];   // Arrays with the screen to go to on button
    void (*fpOnKey[NUMBER_OF_KEYS])(void);	        // Function pointer for each key
	void (*fpOnEntry)(void);		                // Function called on entry
	void (*fpOnExit)(void);	                        // Function called on exit
    i2c_lcd1602_custom_index_t icon;                // Icon to be displayed in the menu on R:1 C:5
};

/*
 * Given a button int
 * The input will be handled by the screen
 */
void abstract_menu_handleKeyInput(int key);

/*
 * Given a starts screen, starts the menu
 */
void abstract_menu_startMenu(MENU_SCREEN_ITEM *startMenu);

// The given menu item will be shown on screen
void abstract_menu_switchScreen(MENU_SCREEN_ITEM *screen);

#endif