#include "TestMenu.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

void test_onEntry(void);
void test_onExit(void);

void test_onLeft(void);
void test_onRight(void);
void test_onBack(void);
void test_onSelect(void);

extern MENU_SCREEN_ITEM secondTestScreen;

//TODO move to implementation file
MENU_SCREEN_ITEM secondTestScreen = {
    // List of all text
    {
        "SECOND",
        "TEST",
        "SCREEN",
        " "
    },
    // Screens to go to on button
    { 
        &secondTestScreen, NULL, NULL, NULL
    },
    // Functions on key
    {
        test_onLeft, test_onRight, test_onSelect, test_onBack
    },
    // On Entry
    test_onEntry,
    // On Exit
    test_onExit
};

//TODO move to implementation file
MENU_SCREEN_ITEM firstMenuScreen = {
    // List of all text
    {
        "HOOFDMENU",
        "CLOCK",
        "RADIO",
        " "
    },
    // Screens to go to on button
    { 
        NULL, &secondTestScreen, NULL, NULL
    },
    // Functions on key
    {
        test_onLeft, test_onRight, test_onSelect, test_onBack
    },
    // On Entry
    test_onEntry,
    // On Exit
    test_onExit
};

void test_onEntry(void)
{
    printf("on entry\n");
}

void test_onExit(void)
{
    printf("on exit\n");
}

void test_onLeft(void)
{
    printf("on Left\n");
}

void test_onRight(void)
{
    printf("on Right\n");
}

void test_onBack(void)
{
    printf("on Back\n");
}

void test_onSelect(void)
{
    printf("on Select\n");
}

void runMenuTest()
{
    printf("menu test\n");

    // Printing the menu
    abstract_menu_startMenu(&firstMenuScreen);

    // Selecting some inputs
    abstract_menu_handleKeyInput(MENU_KEY_SELECT);
    abstract_menu_handleKeyInput(MENU_KEY_BACK);

    abstract_menu_handleKeyInput(MENU_KEY_RIGHT);
    abstract_menu_handleKeyInput(MENU_KEY_LEFT);
    
    abstract_menu_handleKeyInput(MENU_KEY_RIGHT);
    abstract_menu_handleKeyInput(MENU_KEY_LEFT);   

    while (1)
    {
        abstract_menu_handleKeyInput(MENU_KEY_LEFT);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}
