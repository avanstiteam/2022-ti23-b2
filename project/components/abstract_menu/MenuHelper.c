#include <stdio.h>
#include "AbstractMenu.h"
#include "LCDDriver.h"
#include "custom_icon.h"
#include "wifi.h"

// If defined a menu will also be printed to the console
//define PRINT_TEST

#ifdef PRINT_TEST
void print_menu_to_console(MENU_SCREEN_ITEM *screen)
{
    unsigned int lineNr = 0;

    // Going over all the lines in the screen
    for (lineNr = 0; lineNr < MAX_TEXT_LINES; lineNr += 1)
    {
        if (screen->text[lineNr] != NULL)
        {
            printf("%s\n", screen->text[lineNr]);
        }
    }
}
#endif

/*
 * Shows the menu to the LCD screen
 * Screen: MENU_SCREEN_ITEM, the screen to be drawn
 */
void menu_helper_show_menu(MENU_SCREEN_ITEM *screen)
{
// Print the screen to the console
#ifdef PRINT_TEST
    print_menu_to_console(screen);
#endif

    // Clearing the screen
    LCD_Clear_Screen();

    // Going over all the lines in the screen
    if (screen->text[0] != NULL)
        LCD_Write_string_On_ScreenPos(screen->text[0], MID_TOP);

    if (screen->text[1] != NULL)
        LCD_Write_string_On_ScreenPos(screen->text[1], MID_BOTTOM);

    if (screen->text[2] != NULL)
        LCD_Write_string_On_ScreenPos(screen->text[2], LEFT_TOP);

    if (screen->text[3] != NULL)
    {
        LCD_Write_string_On_ScreenPos(screen->text[3], RIGHT_TOP);
    } else if(get_wifi_state() == WIFI_CONNECTED){
        LCD_Write_WIFI_State(true);
    } else if(get_wifi_state() == WIFI_NOT_CONNECTED){
        LCD_Write_WIFI_State(false);
    }
    if (screen->text[4] != NULL)
        LCD_Write_string_On_ScreenPos(screen->text[4], LEFT_BOTTOM);

    if (screen->text[5] != NULL)
        LCD_Write_string_On_ScreenPos(screen->text[5], RIGHT_BOTT0M);

    if (screen->icon != 0)
        LCD_Write_Char_At(screen->icon, 1, 5);



}