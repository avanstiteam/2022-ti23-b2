#ifndef MENU_STRINGS_H

        #define MENU_STRINGS_H
        #define MENU_RADIO_HEADER "RADIO"
        #define RADIO_STATION_PREFIX "Selected station:"
        #define MENU_CLOCK_HEADER "CLOCK"
        #define CLOCK_TIME_PREFIX "Current time:"
        #define CLOCK_DATE_PREFIX "Current date:"
        #define MENU_KARAOKE_HEADER "KARAOKE"
        #define MENU_KARAOKE_SUB_HEADER "make your own party"
        #define MENU_ROTERY_HEADER "ROTERY COLOR PICKER"
        #define MENU_ROTERY_SUB_HEADER "Choose new color!!"

        #define MENU_ALARM_HEADER "SET-UP YOUR ALARM"


        #define ARROW_LEFT "<-"
        #define ARROW_RIGHT "->"
        
        #define BOOT_HEADER_1 "PartyBox"
        #define BOOT_HEADER_2 "Booting Up"
        #define BOOT_HEADER_3 "********************"

        #define RADIO_INFO_MENU_HEADER "-RADIO-"
        #define KARAOKE_INFO_MENU_HEADER "-KAROKE-"
void f();
#endif
