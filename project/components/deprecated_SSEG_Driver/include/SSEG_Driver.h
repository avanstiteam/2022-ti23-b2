#ifndef SEG_DRIVER
#define SEG_DRIVER

    typedef enum DigitNumber {
        DIGIT_ONE = 0x01,
        DIGIT_TWO = 0x02,
        DIGIT_THREE = 0x04,
        DIGIT_FOUR = 0x08
    } SSEG_DigitNumber_t;

    /* Starts writing numbers to seven seg display */
    void SSEG_StartWriting(int core);
    /* Sets the number for the display */
    void SSEG_WriteNumber(int number);
    /* writes the number to the specified digit. Needs the byte value of the digit, not the absolute value. */
    void SSEG_WriteNumberAt(int number, SSEG_DigitNumber_t digitNumber);
    /* writes time to the display */
    void SSEG_WriteTime(int hours, int minutes);
#endif