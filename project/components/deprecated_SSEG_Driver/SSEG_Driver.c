#include <stdio.h>
#include "GPIO_Extender.h"
#include "SSEG_Driver.h"
#include "mcp23017.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#define OFF 10

int number_0 = OFF;
int *number_0p = &number_0;
int number_1 = OFF;
int *number_1p = &number_1;
int number_2 = OFF;
int *number_2p = &number_2;
int number_3 = OFF;
int *number_3p = &number_3;

TaskHandle_t numberTaskHandler = NULL;

static void SSEG_WriteNumbersOnScreen();
static void SSEG_ClearScreen();

int display_Numbers[] = {
    0b00010100,
    0b11010111,
    0b01001100,
    0b01000101,
    0b10000111,
    0b00100101,
    0b00100100,
    0b01010111,
    0b00000100,
    0b00000101,
    0b11111111};

void SSEG_StartWriting(int core)
{
    GPIO_Extender_init();
    xTaskCreatePinnedToCore(&SSEG_WriteNumbersOnScreen, "SegmentWrite", 2048, NULL, 1, numberTaskHandler, core);
}

static void SSEG_WriteNumbersOnScreen()
{
    for (;;)
    {
        SSEG_WriteNumberAt(*number_0p, DIGIT_ONE);
        SSEG_WriteNumberAt(*number_1p, DIGIT_TWO);
        SSEG_WriteNumberAt(*number_2p, DIGIT_THREE);
        SSEG_WriteNumberAt(*number_3p, DIGIT_FOUR);
    }
}

void SSEG_WriteNumberAt(int number, SSEG_DigitNumber_t digitNumber)
{
    SSEG_ClearScreen();
    vTaskDelay(5 / portTICK_PERIOD_MS);
    GPIO_Extender_write_register(MCP23017_GPIO, GPIOA, digitNumber);
    vTaskDelay(5 / portTICK_PERIOD_MS);
    GPIO_Extender_write_register(MCP23017_GPIO, GPIOB, number);
    vTaskDelay(5 / portTICK_PERIOD_MS);
}

void SSEG_WriteNumber(int number)
{
    if (number < 10)
    {
        *number_0p = display_Numbers[number];
        *number_1p = display_Numbers[OFF];
        *number_2p = display_Numbers[OFF];
        *number_3p = display_Numbers[OFF];
    }
    else if (number < 100)
    {
        *number_0p = display_Numbers[number % 10];
        *number_1p = display_Numbers[(number % 100) / 10];
        *number_2p = display_Numbers[OFF];
        *number_3p = display_Numbers[OFF];
    }
    else if (number < 1000)
    {
        *number_0p = display_Numbers[number % 10];
        *number_1p = display_Numbers[(number % 100) / 10];
        *number_2p = display_Numbers[(number % 1000) / 100];
        *number_3p = display_Numbers[OFF];
    }
    else
    {
        *number_0p = display_Numbers[number % 10];
        *number_1p = display_Numbers[(number % 100) / 10];
        *number_2p = display_Numbers[(number % 1000) / 100];
        *number_3p = display_Numbers[(number % 10000) / 1000];
    }
}

void SSEG_WriteTime(int hours, int minutes)
{
    if (hours < 10)
    {
        *number_2p = display_Numbers[hours] & 0b11111011;
        *number_3p = display_Numbers[0];
    }
    else
    {
        *number_2p = display_Numbers[hours % 10] & 0b11111011;
        *number_3p = display_Numbers[(int)(hours / 10)];
    }

    if (minutes < 10)
    {
        *number_0p = display_Numbers[minutes];
        *number_1p = display_Numbers[0];
    }
    else
    {
        *number_0p = display_Numbers[minutes % 10];
        *number_1p = display_Numbers[(int)(minutes / 10)];
    }
}

/* Clears screen for writing again */
static void SSEG_ClearScreen()
{
    GPIO_Extender_write_register(MCP23017_GPIO, GPIOB, 0b11111111);
}