/* HTTPS GET Example using plain mbedTLS sockets
 *
 * Contacts the howsmyssl.com API via TLS v1.2 and reads a JSON
 * response.
 *
 * Adapted from the ssl_client1 example in mbedtls.
 *
 * Original Copyright (C) 2006-2016, ARM Limited, All Rights Reserved, Apache 2.0 License.
 * Additions Copyright (C) Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD, Apache 2.0 License.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Changed by Luca Brugel to fit the project */
#include <string.h>
#include <stdlib.h>
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_netif.h"
#include "HTTP_Get_Module.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "esp_tls.h"
#include "esp_crt_bundle.h"

/* Constants that aren't configurable in menuconfig */
#define WEB_SERVER "lyrics.ewoutbaars.technology"
#define WEB_PORT "443"
#define WEB_URL "https://lyrics.ewoutbaars.technology/getLyrics?song="

static const char *TAG = "HTTP_GET";


/**
 * @brief Sends a HTTP request to the api server, this will get the lyrics and send it back
 * 
 * @param artistAndSong artist and song formatted to ARTIST-SONG
 * @param result this is the result string
 * @return esp_err_t returns error or ESP_OK
 */
esp_err_t https_get(char *artistAndSong, char *result)
{
    
    char url[strlen(WEB_URL) + strlen(artistAndSong) - 1];
    sprintf(url, "%s%s", WEB_URL, artistAndSong); 
    char buf[512];
    int ret, len;
    bool startReading = false;
    char *stdRequest = "GET  HTTP/1.0\r\n"
    "Host: "WEB_SERVER"\r\n"
    "User-Agent: esp-idf/1.0 esp32\r\n"
    "\r\n";
    char REQUEST[strlen(stdRequest) + strlen(url)];
    sprintf(REQUEST, "GET %s HTTP/1.0\r\n"
    "Host: "WEB_SERVER"\r\n"
    "User-Agent: esp-idf/1.0 esp32\r\n"
    "\r\n", url);
    ESP_LOGI("HTTP", "Starting while");
    int index = 0;
    while(1) {
        esp_tls_cfg_t cfg = {
            .crt_bundle_attach = esp_crt_bundle_attach,
        };
        ESP_LOGI("HTTP", "SENDIng");
        struct esp_tls *tls = esp_tls_conn_http_new(url, &cfg);
        ESP_LOGI("HTTP", "connection");
        if(tls != NULL) {
            ESP_LOGI(TAG, "Connection established...");
        } else {
            ESP_LOGE(TAG, "Connection failed...");
            return ESP_FAIL;
        }

        size_t written_bytes = 0;
        do {
            ret = esp_tls_conn_write(tls,
                                     REQUEST + written_bytes,
                                     strlen(REQUEST) - written_bytes);
            if (ret >= 0) {
                ESP_LOGI(TAG, "%d bytes written", ret);
                written_bytes += ret;
            } else if (ret != ESP_TLS_ERR_SSL_WANT_READ  && ret != ESP_TLS_ERR_SSL_WANT_WRITE) {
                ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
                goto exit;
            }
        } while(written_bytes < strlen(REQUEST));

        ESP_LOGI(TAG, "Reading HTTP response...");

        do
        {
            len = sizeof(buf) - 1;
            bzero(buf, sizeof(buf));
            ret = esp_tls_conn_read(tls, (char *)buf, len);

            if(ret == ESP_TLS_ERR_SSL_WANT_WRITE  || ret == ESP_TLS_ERR_SSL_WANT_READ)
                continue;

            if(ret < 0)
           {
                ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
                break;
            }

            if(ret == 0)
            {
                ESP_LOGI(TAG, "connection closed");
                break;
            }

            len = ret;
            ESP_LOGD(TAG, "%d bytes read", len);
            /* Print response directly to stdout as it is read */
            for(int i = 0; i < len; i++) {
                if(startReading){
                    result[index] = buf[i];
                    index++;
                } else if(buf[i] == '[') {
                    startReading = true;
                    result[index] = buf[i];
                    index++;
                }
            }
        } while(1);

    exit:
        esp_tls_conn_delete(tls);
        ESP_LOGI("STR", "STR: %s", result);
        result[index] = '\0';
        break;
    }
    return ESP_OK;
}

