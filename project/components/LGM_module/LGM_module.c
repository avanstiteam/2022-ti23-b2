/**
 * @file LGM_module.c
 * @author Luca Brugel
 * @brief Module for filling a espressif queue with karaoke lyrics
 * @version 0.1
 * @date 2022-03-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include <stdio.h>
#include "LGM_module.h"

static const char *TAG = "LGM";

static LGM_error_t LGM_Send_HTTP_Request(char *artistAndSong, char *fullstring);
static LGM_error_t LGM_Convert_String_To_GKAR_Lyrics_line_t(char *string);
static LGM_error_t LGM_Add_SingleLine_To_Queue(GKAR_Lyrics_line_t *lyricsLine);
static void LGM_Write_Error(LGM_error_t error);
/**
 * @brief Fills the lyrics queue with GKAR_lyrics_lines_t with the requested song.
 * 
 * @param artist The artist from the song.
 * @param song The song title.
 * @return LGM_error_t returns an error message if the request failed. Will return 0 if the request was succesfull.
 */
void LGM_Fill_LQueue(void *artistAndSong){
    char *fullHttpString = (char *) malloc(sizeof(char) * 5000);
    
    LGM_error_t error = LGM_Send_HTTP_Request((char *)artistAndSong, fullHttpString);
    if(error != LGM_OK){ 
        LGM_Write_Error(error);
    } 

    /* Try to convert the full http string to the correct GKAR lyrics line struct then add it to the queue */
    error = LGM_Convert_String_To_GKAR_Lyrics_line_t(fullHttpString);
    if(error != LGM_OK){
        LGM_Write_Error(error);
    }
    free(fullHttpString);
    vTaskDelete(NULL);
}

/**
 * @brief Will send a HTTP request to a remote server for getting lyrics, will fill a string
 * 
 * @param artistAndSong This is the string formatted in ARTIST-SONG
 * @param fullString This string will be filled with the full string of the http request.
 * @return LGM_error_t will return an error message. Will return 0 if succesfull.
 */
static LGM_error_t LGM_Send_HTTP_Request(char *artistAndSong, char *fullString){
    /* Will send an http request to the api */
    esp_err_t error = https_get(artistAndSong, fullString);
    if(error == ESP_OK){
        return LGM_OK;
    } else {
        return LGM_HTTP_REQUEST_FAILED;
    }
}

/**
 * @brief Converts full string to GKAR_Lyrics_line_t and adds it to the queue
 * 
 * @param string The full string unparsed
 * @return LGM_error_t returns an error if failed. Will return 0 if succesfull.
 */
static LGM_error_t LGM_Convert_String_To_GKAR_Lyrics_line_t(char *string){
    char tempString[50];
    int indexTempString = 0;
    bool isTimeString = false;
    ESP_LOGI("STR", "%s", string);

    int minutes = -1;
    int seconds = -1;
    int miliseconds = -1;
    long time = 0;

    bool startReading = false;
    /* Checks if string is done */
    for(int i = 1; i < strlen(string); i++){
        /* checks if end of line */
        if(string[i] != '\n' && startReading){
            /* checks if the string is a timestamp */
            if(string[i] == '['){
                isTimeString = true;

            } else if (string[i] == ']'){
                miliseconds = atoi(tempString);
                isTimeString = false;
                indexTempString = 0;
                memset(tempString, '\0', sizeof(tempString));
                
                /* checks if the ints are -1 and sets the to zero for math */
                if(minutes == -1) minutes = 0;
                if(seconds == -1) seconds = 0;
                if(miliseconds == -1) miliseconds = 0;
                
                /* sets the time to miliseconds */
                time = (minutes * 60 * 1000) + (seconds * 1000) + (miliseconds * 10);

                /* sets all the time specifiers to -1 */
                minutes = -1;
                seconds = -1;
                miliseconds = -1;
            }

            /* Will write out the timestamp in miliseconds */
            if(isTimeString){
                if(string[i] != ' ' && string[i] != '['){
                    if(string[i] != ':' && string[i] != '.'){
                        tempString[indexTempString] = string[i];
                        indexTempString++;
                    } else {
                        if(minutes == -1){
                            minutes = atoi(tempString);
                        } else if(seconds == -1){
                            seconds = atoi(tempString);
                        }   
                            
                        memset(tempString, '\0', sizeof(tempString) * sizeof(char));
                        indexTempString = 0;
                    }
                }
            } else if(string[i] != ']'){
                // Will add the next char to an string if it's not a time notation
                tempString[indexTempString] = string[i];
                indexTempString++;
            }
        } else {
            if(startReading){
                // Will paste the string value and allocate memory for useing later. The pointer will be added to a queue for reading an writing on the screen
                char lyricString[strlen(tempString) * sizeof(char)];
                strcpy(lyricString, tempString);
                vTaskDelay(10);
                char * stringpointer = (char *) malloc((strlen(lyricString) + 1) * sizeof(char));
                stringpointer = memcpy(stringpointer, lyricString, (strlen(lyricString) + 1) * sizeof(char));
                GKAR_Lyrics_line_t lyrics = {stringpointer, time};

                // Will clear the string value and start writing in it again.
                memset(tempString, '\0', sizeof(tempString));
                indexTempString = 0;
                time = 0;
                LGM_error_t err = LGM_Add_SingleLine_To_Queue(&lyrics);
                if(err != LGM_OK){
                    return err;
                }
            }

            // Will remove the first few lines in a HTTP request which are not lyrics
            if((string[i] == '\n' && string[i-1] == '\n') || (string[i-1] == '\r' && string[i] == '\n' && string[i-2] == '\n' && string[i-3] == '\r')) startReading = true;

        }
        
    }

    // Will paste the string value and allocate memory for useing later. The pointer will be added to a queue for reading an writing on the screen
    char lyricString[strlen(tempString) * sizeof(char)];
    strcpy(lyricString, tempString);
    char * stringpointer = (char *) malloc((strlen(lyricString) + 1) * sizeof(char));
    stringpointer = memcpy(stringpointer, lyricString, (strlen(lyricString) + 1) * sizeof(char));
    GKAR_Lyrics_line_t lyrics = {stringpointer, time};
    LGM_error_t err = LGM_Add_SingleLine_To_Queue(&lyrics);
    if(err != LGM_OK){
        return err;
    }
    return LGM_OK;
}

/**
 * @brief Adds a single lyrics line to the lyrics queue
 * 
 * @param lyricsLine The line that needs to be added to the queue
 * @return LGM_error_t returnes an error if failed. Returnes 0 if succesfull
 */
static LGM_error_t LGM_Add_SingleLine_To_Queue(GKAR_Lyrics_line_t *lyricsLine){
    vTaskDelay(1 / portTICK_PERIOD_MS);
    LyricsHandlingQueue *queuehandle = GKAR_GetLyricsQueueHandle();
    xQueueSend(*queuehandle, lyricsLine, portMAX_DELAY);
    
    return LGM_OK;
}

/**
 * @brief Will write an error when one occurs
 * @param error the error message that came back
 */
static void LGM_Write_Error(LGM_error_t error){
    switch (error)
    {
    case LGM_SONG_NOT_FOUND:
    ESP_LOGE(TAG, "Song not found. Check your song name.");    
    break;

    case LGM_HTTP_REQUEST_FAILED:
    ESP_LOGE(TAG, "HTTP request failed. Check connection or link.");
    break;

    case LGM_LYRICS_CONVERSION_FAILED:
    ESP_LOGE(TAG, "Lyrics conversion failed, check your lyrics file.");
    break;

    case LGM_ADD_TO_QUEUE_FAILED:
    ESP_LOGE(TAG, "Adding Lyrics to the queue failed.");
    break;

    case LGM_OK:
    break;
    }
}