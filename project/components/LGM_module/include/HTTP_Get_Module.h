#ifndef HTTP_Get_Module
#define HTTP_Get_Module

    /**
     * @brief Sends a HTTP request to the api server, this will get the lyrics and send it back
     * 
     * @param artistAndSong artist and song formatted to ARTIST-SONG
     * @param result this is the result string
     * @return esp_err_t returns error or ESP_OK
     */
    esp_err_t https_get(char *artistAndSong, char *result);

#endif