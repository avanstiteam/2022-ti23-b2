/**
 * @file LGM_module.h
 * @author Luca Brugel
 * @brief Module for filling a espressif queue with karaoke lyrics
 * @version 0.1
 * @date 2022-03-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef LGM_Module
#define LGM_Module

#include "GKAR_General_Karaoke.h"
#include "string.h"
#include "ctype.h"
#include "esp_log.h"
#include "HTTP_Get_Module.h"
#include "stdlib.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
    /**
     * @brief All error messages which can be returned by this module.
     * 
     */
    typedef enum ERROR_MESSAGES {
        LGM_OK = 0,
        LGM_SONG_NOT_FOUND = 1,
        LGM_HTTP_REQUEST_FAILED = 2,
        LGM_LYRICS_CONVERSION_FAILED = 3,
        LGM_ADD_TO_QUEUE_FAILED = 4
    } LGM_error_t;

    /**
     * @brief Fills the lyrics queue with GKAR_lyrics_lines_t with the requested song.
     * 
     * @param artistAndSong The is the artist and song formatted in ARTIST-SONG
     * 
     * @return returns an error message if the request failed. Will return 0 if the request was succesfull.
     */
    void LGM_Fill_LQueue(void *artistAndSong);

#endif