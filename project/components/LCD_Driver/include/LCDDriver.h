#include <stdbool.h>
#include "LCDUtil.h"
#ifndef LCDDRIVER

#define LCD_NUM_ROWS               4
#define LCD_NUM_COLUMNS            32
#define LCD_NUM_VISIBLE_COLUMNS    20

    typedef enum {
        LEFT,
        RIGHT
    } cursor_direction_t;

    /* Defines the different spaces on the screen */
    typedef enum {
        LEFT_TOP,
        RIGHT_TOP,
        MID_TOP,
        MID_BOTTOM,
        LEFT_BOTTOM,
        RIGHT_BOTT0M
    } screen_pos_t;

    /* Safes data about the screen position */
    typedef struct {
        int row;
        int col;
    } screen_space_info_t;

    // Max lenght the LCD can display on screen
    #define MAX_STRING_LENTGH 20

    /* Sets up LCD driver for writing */
    void LCD_setup(void);

    /* Toggles backlight on or off */
    void LCD_Backlight_toggle(bool state);

    /* Toggles cursor on or off */
    void LCD_Cursor_toggle(bool state);

    /* Writes a char at a specific position */
    void LCD_Write_Char_At(char c, int row, int col);

    /* Writes a string at a specific position */
    void LCD_Write_String_At(const char * c, int row, int col);
    
    /* Toggles blinking cursor */
    void LCD_Blinking_Cursor_toggle(bool state);

    /* Moves cursor left or right with an given amount */
    void LCD_Cursor_Move(cursor_direction_t dir, int amount);

    /* Toggles scrolling of the screen */
    void LCD_Auto_Scrolling_toggle(bool state);

    /* Defines a new custom character at the given index
       Index has to be:
            I2C_LCD1602_CHARACTER_CUSTOM_0, 
            I2C_LCD1602_CHARACTER_CUSTOM_1, 
            I2C_LCD1602_CHARACTER_CUSTOM_2, 
            I2C_LCD1602_CHARACTER_CUSTOM_3, 
            I2C_LCD1602_CHARACTER_CUSTOM_4, 
            I2C_LCD1602_CHARACTER_CUSTOM_5, 
            I2C_LCD1602_CHARACTER_CUSTOM_6, 
            I2C_LCD1602_CHARACTER_CUSTOM_7, 
    */
    void LCD_Define_Custom_Char(uint8_t character[], i2c_lcd1602_custom_index_t index);

    /* Clears entire screen */
    void LCD_Clear_Screen();

    /* Clears a row */
    void LCD_Clear_Row(int row);

    /* Write a string in the middle of the screen */
    void LCD_Write_String_Middle(const char * str, int row);

    /* Writes a string at a certain screen position */
    void LCD_Write_string_On_ScreenPos(const char * string, screen_pos_t position);

    void LCD_Write_WIFI_State(bool state);
#endif