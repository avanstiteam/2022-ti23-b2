/*
 *   @autor Luca Brugel
 *   Created for a Computer Science school project called Embeded System for AVANS school of applied sciences
 *
 *   Used code written by David Antliff
 */

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "esp32/rom/uart.h"
#include <string.h>

#include "smbus.h"
#include "LCDUtil.h"
#include "LCDDriver.h"
#include "custom_icon.h"

//#define TAG "LCDDRIVER"
static const char *TAG = "LCDDRIVER";


#define I2C_MASTER_NUM I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN 0 // disabled
#define I2C_MASTER_RX_BUF_LEN 0 // disabled
#define I2C_MASTER_FREQ_HZ 100000
#define I2C_MASTER_SDA_IO 18
#define I2C_MASTER_SCL_IO 23
#define LCD_ADDRESS_NUMBER 0x27

i2c_port_t i2c_port = 0;
i2c_lcd1602_info_t *lcd_info = NULL;

static void LCD_Write_Right_to_Lelft(const char *string, int row, int col);

/* Defines spaces on the screen to write characters */
screen_space_info_t LCD_screen_positions_a[] = {
    {0, 0},
    {0, 10},
    {1, 10},
    {2, 10},
    {3, 0},
    {3, 10}};

uint8_t lcd_address = 0;
    
/*  Coppied from original code made by David Antliff
 *   used to initialize the LCD using the SMBUS and i2c
 */
static void i2c_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE; // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE; // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);
}

SemaphoreHandle_t xSemaphore = NULL;

/* Sets up LCD driver for writing */
void LCD_setup(void)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);

    i2c_init();                           // Init I2C
    i2c_port_t i2c_port = I2C_MASTER_NUM; // Set the I2C ports and adress
    uint8_t lcd_address = LCD_ADDRESS_NUMBER;

    // set up smbus
    smbus_info_t *smbus_info = smbus_malloc();
    ESP_ERROR_CHECK(smbus_init(smbus_info, i2c_port, lcd_address));
    ESP_ERROR_CHECK(smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS));

    // set up LCD with backlight off
    lcd_info = i2c_lcd1602_malloc();
    ESP_ERROR_CHECK(i2c_lcd1602_init(lcd_info, smbus_info, true,
                                     LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VISIBLE_COLUMNS));

    ESP_ERROR_CHECK(i2c_lcd1602_reset(lcd_info));

    xSemaphore = xSemaphoreCreateMutex();
}

/* Toggles backlight on or off */
void LCD_Backlight_toggle(bool state)
{
    i2c_lcd1602_set_backlight(lcd_info, state);
}

/* Toggles cursor on or off */
void LCD_Cursor_toggle(bool state)
{
    i2c_lcd1602_set_cursor(lcd_info, true);
}

/* Writes a char at a specific position */
void LCD_Write_Char_At(char c, int row, int col)
{
    if (xSemaphore != NULL)
    {
        /* See if we can obtain the semaphore.  If the semaphore is not
        available wait 100 ticks to see if it becomes free. */
        if (xSemaphoreTake(xSemaphore, (TickType_t)100) == pdTRUE)
        {
            i2c_lcd1602_move_cursor(lcd_info, col, row);
            i2c_lcd1602_write_char(lcd_info, c);
            xSemaphoreGive(xSemaphore);
        }
        else
        {
            ESP_LOGE(TAG,"could not obtain the semaphore and can therefore not access the shared resource safely");
            /* We could not obtain the semaphore and can therefore not access
            the shared resource safely. */
        }
    }
}

/* Writes a string at a specific position */
void LCD_Write_String_At(const char *c, int row, int col)
{
    int length = strlen(c);

    if (length <= MAX_STRING_LENTGH)
    {
        for (int i = 0; i < length; i++)
        {
            LCD_Write_Char_At(c[i], row, col + i);
        }
    }
    else
    {
        for (int i = 0; i < MAX_STRING_LENTGH; i++)
        {
            LCD_Write_Char_At(c[i], row, col + i);
        }
    }
}

/* Toggles blinking cursor */
void LCD_Blinking_Cursor_toggle(bool state)
{
    i2c_lcd1602_set_blink(lcd_info, state);
}

/* Moves cursor left or right with an given amount */
void LCD_Cursor_Move(cursor_direction_t dir, int amount)
{
    if (dir == LEFT)
    {
        for (int i = 0; i < amount; i++)
        {
            i2c_lcd1602_move_cursor_left(lcd_info);
        }
    }
    else
    {
        for (int i = 0; i < amount; i++)
        {
            i2c_lcd1602_move_cursor_right(lcd_info);
        }
    }
}

/* Toggles scrolling of the screen */
void LCD_Auto_Scrolling_toggle(bool state)
{
    i2c_lcd1602_set_auto_scroll(lcd_info, state);
}

/* Defines a new custom character at the given index
       Index has to be:
            I2C_LCD1602_CHARACTER_CUSTOM_0,
            I2C_LCD1602_CHARACTER_CUSTOM_1,
            I2C_LCD1602_CHARACTER_CUSTOM_2,
            I2C_LCD1602_CHARACTER_CUSTOM_3,
            I2C_LCD1602_CHARACTER_CUSTOM_4,
            I2C_LCD1602_CHARACTER_CUSTOM_5,
            I2C_LCD1602_CHARACTER_CUSTOM_6,
            I2C_LCD1602_CHARACTER_CUSTOM_7,
*/
void LCD_Define_Custom_Char(uint8_t character[], i2c_lcd1602_custom_index_t index)
{
    i2c_lcd1602_define_char(lcd_info, index, character);
}

/* Clears entire screen */
void LCD_Clear_Screen()
{
    i2c_lcd1602_clear(lcd_info);
}

/* Clears a row */
void LCD_Clear_Row(int row)
{
    for (int i = 0; i < LCD_NUM_COLUMNS; i++)
    {
        LCD_Write_Char_At(' ', row, i);
    }
    i2c_lcd1602_move_cursor(lcd_info, 0, row);
}

/* Write a string in the middle of the screen */
void LCD_Write_String_Middle(const char *str, int row)
{
    int middle = LCD_NUM_VISIBLE_COLUMNS / 2;
    int halfOfString = strlen(str) / 2;
    LCD_Write_String_At(str, row, middle - halfOfString);
}

void LCD_Write_WIFI_State(bool state)
{
    if (state == true)
    {
        LCD_Write_Char_At(INDEX_CONNECTED_ICON_1, 0, 17);
        LCD_Write_Char_At(INDEX_CONNECTED_ICON_2, 0, 18);
        LCD_Write_Char_At(INDEX_CONNECTED_ICON_3, 0, 19);
    }
    else
    {
        LCD_Write_Char_At('_', 0, 17);
        LCD_Write_Char_At('_', 0, 18);
        LCD_Write_Char_At('_', 0, 19);
    }
}

/* Writes a string at a certain screen position */
void LCD_Write_string_On_ScreenPos(const char *string, screen_pos_t position)
{
    if (position == MID_BOTTOM || position == MID_TOP)
    {
        LCD_Write_String_Middle(string, LCD_screen_positions_a[(int)position].row);
    }
    else if (position == RIGHT_BOTT0M || position == RIGHT_TOP)
    {
        LCD_Write_Right_to_Lelft(string, LCD_screen_positions_a[(int)position].row, LCD_screen_positions_a[(int)position].col);
    }
    else
    {
        LCD_Write_String_At(string, LCD_screen_positions_a[(int)position].row, LCD_screen_positions_a[(int)position].col);
    }
}

/*
 * Given a string, row and col
 * Text will be writen from left to right (backwards)
 */
static void LCD_Write_Right_to_Lelft(const char *string, int row, int col)
{
    int length = strlen(string);
    if (length <= MAX_STRING_LENTGH)
    {
        for (int i = 0; i < strlen(string); i++)
        {
            LCD_Write_Char_At(string[i], row, (LCD_NUM_VISIBLE_COLUMNS - length) + i);
        }
    }
    else
    {
        for (int i = 0; i < MAX_STRING_LENTGH; i++)
        {
            LCD_Write_Char_At(string[i], row, (LCD_NUM_VISIBLE_COLUMNS - 10) + i);
        }
    }
}