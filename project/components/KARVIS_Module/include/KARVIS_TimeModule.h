#ifndef KARVIS_TIME_MODULE_H
#define KARVIS_TIME_MODULE_H

int KARVIS_HasTimeBeenReached();

void KARVIS_StartTime();

void KARVIS_Reset();

void KARVIS_SetTarget( long millis );

#endif