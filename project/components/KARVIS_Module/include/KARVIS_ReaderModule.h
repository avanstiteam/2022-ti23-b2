#ifndef KARVIS_READER_MODULE_H
#define KARVIS_READER_MODULE_H

#include "GKAR_General_Karaoke.h"

/**
 * @brief Method will start reading the queue from the GKAR module to get all the
 * lyrics lines that need to be displayed
 * 
 */
void KARVIS_StartReading();

/**
 * @brief Method will stop reading the queue from the GKAR module
 * 
 */
void KARVIS_StopReading();

/**
 * @brief This method set the action that is called when a Lyrics line is received from the queue.
 * 
 * @param fOnAction The action to call on queue item
 */
void KARVIS_SetOnAction(void ( *fOnAction)(GKAR_Lyrics_line_t) );

#endif