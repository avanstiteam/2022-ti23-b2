#include "KARVIS_DisplayModule.h"

#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "LCDDriver.h"

#define MAX_LINES 3
#define STARTING_LINE 1 

void KARVIS_DisplayLyrics(char *text)
{
    int lastWordPos = 0;

    int lastWritePos = 0;
    int currentChar = 0;

    // Makes shure the last character is not a random character
    text[strlen(text) - 1] = '\0';

    // Clear the LCD
    LCD_Clear_Screen();

    // Go over all the lines on the display
    for ( int i = STARTING_LINE; i < MAX_LINES + STARTING_LINE; i++)
    {
        if ( text[currentChar] == '\0' ) return;

        // Loop until the string or line is over
        while ( text[currentChar] != '\0' && ( currentChar - lastWritePos) <= (LCD_NUM_VISIBLE_COLUMNS - 1))
        {
            // Check if the next char is a space
            if ( text[currentChar + 1] == ' ') lastWordPos = currentChar + 1;

            // To next char
            currentChar++;
        }

        // After the loop write all the remaining strings
        char lineBlock[LCD_NUM_VISIBLE_COLUMNS + 1];

        memset( lineBlock, '\0', sizeof(lineBlock));

        int printSize = lastWordPos - lastWritePos;

        // Check if the lenght is under 0 print full string rest
        if ( text[currentChar] == '\0' ) printSize = ( currentChar - lastWritePos );
        
        strncpy( lineBlock, (text + lastWritePos), printSize );

        // Print to the LCD
        LCD_Write_String_Middle( lineBlock, i );

        ESP_LOGV("TAG", "Got the lyrics line with stats: lastwordPos: %d, lastWritePos: %d, text: %s",lastWordPos, lastWritePos, lineBlock);

        //Updating the lastWritePos to this write ( +1 to account for the spacebar )
        lastWritePos = lastWordPos +1 ;

    }

}
