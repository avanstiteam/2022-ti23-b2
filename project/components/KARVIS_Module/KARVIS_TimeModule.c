#include "KARVIS_TimeModule.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"

TickType_t ticks = 0;

long millisTarget = 0;

int KARVIS_HasTimeBeenReached()
{
    if ( ticks == 0) KARVIS_Reset();

    TickType_t ticksDifference = ( xTaskGetTickCount() - ticks) * 10;
    ESP_LOGV("TAG", "Time module, Ticks:%u, Ticks diff:%u, target: %ld", ticks, ticksDifference, millisTarget);
    return ticksDifference >= millisTarget;
}


void KARVIS_StartTime()
{
    KARVIS_Reset();
}


void KARVIS_Reset()
{
    ticks = xTaskGetTickCount();
}

void KARVIS_SetTarget( long millis )
{
    ESP_LOGI("TAG", "Time target set to: %ld", millis);
    millisTarget = millis;
}