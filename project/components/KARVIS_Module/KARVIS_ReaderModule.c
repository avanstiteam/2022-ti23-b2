#include "KARVIS_ReaderModule.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "KARVIS_DisplayModule.h"
#include "KARVIS_TimeModule.h"

// Defines
#define EMPTY_QUEUE_DELAY 500

static const char *TAG = "KARVIS_MODULE";

// The set functionpointer on call
// Functionpointer can only be set from the set method and read from the reader, so semaphores are not needed
void ( *fOnAction)(GKAR_Lyrics_line_t) = NULL;

// Taskhandle for the task reading the Queue from GKAR module
TaskHandle_t readerTaskHandle;

// Defining stagic methods
static void QueueReadingTask( void *parameters );

/**
 * @brief Method will start reading the queue from the GKAR module to get all the
 * lyrics lines that need to be displayed
 * 
 */
void KARVIS_StartReading()
{
    // Getting the queue
    LyricsHandlingQueue *queueHandler = GKAR_GetLyricsQueueHandle();

    // Creating the task
    xTaskCreate(QueueReadingTask, "Lyrics queue reading task", 8192, (void *)queueHandler, 2, readerTaskHandle);
}

/**
 * @brief Method will stop reading the queue from the GKAR module
 * 
 */
void KARVIS_StopReading()
{
    // Checking if taskHandle exists
    if ( readerTaskHandle == NULL) return;

    // Killing the created Task
    vTaskDelete( readerTaskHandle );

}

/**
 * @brief This method set the action that is called when a Lyrics line is received from the queue.
 * 
 * @param fOnAction The action to call on queue item
 */
void KARVIS_SetOnAction(void ( *fOnActionCall)(GKAR_Lyrics_line_t) )
{
    // Updateing the functionpointer
    fOnAction = fOnActionCall;
}

/**
 * @brief The task that will read the queue and call the set functionpointer when a message arrives
 * 
 * @param parameters The queue to read the message from
 */
static void QueueReadingTask( void *parameters )
{
    // Getting the Queue needed from the passed parameters
    LyricsHandlingQueue *lyricsQueue = (LyricsHandlingQueue *)parameters;

    // Defining an item to copy the queue items into
    GKAR_Lyrics_line_t queueItemHolder;
    int holderUpdated = 0;

    KARVIS_StartTime();

    // Starting the reading loop for the queue
    for ( ;; )
    {
        // Delay for Task space
        vTaskDelay(50 / portTICK_RATE_MS);


        // Breaking if Queue is NULL
        if ( lyricsQueue == NULL ) break;

        // Draw the buffer if time has been reached, else continue
        if( !KARVIS_HasTimeBeenReached() && queueItemHolder.text != NULL)
        {
            // Print the itemholder if the drawn is the first time

            if ( holderUpdated )
            {
                KARVIS_DisplayLyrics(queueItemHolder.text);
                free(queueItemHolder.text);

                holderUpdated = 0;
            }

            // Continue will go back to the start of the loop
            continue;
        }
        
        // Checking if we can get a value from the queue
        if ( xQueueReceive ( *lyricsQueue, &queueItemHolder, portMAX_DELAY) == pdPASS ) // Ok if pdPass is returned 
        {
            ESP_LOGI(TAG, "Lyrics queue read value:  %ld, %s", queueItemHolder.timestamp, queueItemHolder.text);

            //Checking if functionpointer is set
            if ( (*fOnAction) != NULL )
            {
                // Calling the set onAction pointer
                (*fOnAction)(queueItemHolder);
            }

            holderUpdated = 1;
            KARVIS_SetTarget(queueItemHolder.timestamp);
        }
    }

    // Killing the task on break
    vTaskDelete( NULL );
}
