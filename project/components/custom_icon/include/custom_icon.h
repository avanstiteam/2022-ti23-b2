#include "LCDUtil.h"
#ifndef CUSTOM_ICONS_H
    #define CUSTOM_ICONS_H
        #define MUSIC_NOTE_ICON {0b00100,0b00110,0b00101,0b00100,0b00100,0b11100,0b11100,0b11100}
        #define CONNECTED_ICON_1 {0b00000,0b00000,0b00000,0b00000,0b00000,0b11111,0b11111,0b11111}
        #define CONNECTED_ICON_2 {0b00000,0b00000,0b11111,0b11111,0b11111,0b11111,0b11111,0b11111}
        #define CONNECTED_ICON_3 {0b11111,0b11111,0b11111,0b11111,0b11111,0b11111,0b11111,0b11111}
        #define RADIO_ICON {0b00000,0b00010,0b00100,0b01000,0b10000,0b11111,0b10101,0b11111}
        #define CLOCK_ICON {0b00000,0b01110,0b10101,0b10101,0b10111,0b10001,0b01110,0b00000}
        #define AUDIO_ICON {0B00010,0B00110,0B01110,0B11110,0B11110,0B01110,0b00110,0b00010}

        #define INDEX_MUSIC_NOTE_ICON I2C_LCD1602_INDEX_CUSTOM_1
        #define INDEX_CONNECTED_ICON_1 I2C_LCD1602_INDEX_CUSTOM_2
        #define INDEX_CONNECTED_ICON_2 I2C_LCD1602_INDEX_CUSTOM_3
        #define INDEX_CONNECTED_ICON_3 I2C_LCD1602_INDEX_CUSTOM_4
        #define INDEX_RADIO_ICON I2C_LCD1602_INDEX_CUSTOM_5
        #define INDEX_CLOCK_ICON I2C_LCD1602_INDEX_CUSTOM_6
        #define INDEX_SOUND_ICON I2C_LCD1602_INDEX_CUSTOM_7

void setup_Icons();        
#endif
