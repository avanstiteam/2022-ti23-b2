#include "custom_icon.h"
#include "LCDDriver.h"




void setup_Icons(){
    uint8_t chars[] = MUSIC_NOTE_ICON;
    LCD_Define_Custom_Char(chars, INDEX_MUSIC_NOTE_ICON);
    uint8_t chars1[] = RADIO_ICON;
    LCD_Define_Custom_Char(chars1, INDEX_RADIO_ICON);
    uint8_t chars2[] = CONNECTED_ICON_1;
    LCD_Define_Custom_Char(chars2, INDEX_CONNECTED_ICON_1);
    uint8_t chars3[] = CONNECTED_ICON_2;
    LCD_Define_Custom_Char(chars3, INDEX_CONNECTED_ICON_2);
    uint8_t chars4[] = CONNECTED_ICON_3;
    LCD_Define_Custom_Char(chars4, INDEX_CONNECTED_ICON_3);
    uint8_t chars5[] = CLOCK_ICON;
    LCD_Define_Custom_Char(chars5, INDEX_CLOCK_ICON);
    uint8_t chars6[] = AUDIO_ICON;
    LCD_Define_Custom_Char(chars6, INDEX_SOUND_ICON);
}