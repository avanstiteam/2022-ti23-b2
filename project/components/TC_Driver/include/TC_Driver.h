#ifndef TC_DRIVER
#define TC_DRIVER

    #define MAIN_PATH "file://sdcard/klok"
    #define MAIN_SUFFIX ".wav"
    #define HOUR_SUFFIX "uur"

    void TC_Speak_Time(int hours, int minutes);

#endif