#include "TC_Driver.h"
#include "Audio_Handler.h"
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

void TC_Speak_Time(int hours, int minutes){
    char fileUrl[35];
    if(hours != 0){
        sprintf(fileUrl, "%s%d%s", MAIN_PATH, hours, MAIN_SUFFIX);
        printf(fileUrl);
        xQueueSend(talking_clock_queue, fileUrl, portMAX_DELAY);
    } else {
        sprintf(fileUrl, "%s%d%s", MAIN_PATH, 12, MAIN_SUFFIX);
        xQueueSend(talking_clock_queue, fileUrl, portMAX_DELAY);
    }

    sprintf(fileUrl, "%s%s%s", MAIN_PATH, HOUR_SUFFIX, MAIN_SUFFIX);
    printf(fileUrl);
    xQueueSend(talking_clock_queue, fileUrl, portMAX_DELAY);
    if(minutes > 0){
        sprintf(fileUrl, "%s%d%s", MAIN_PATH, minutes, MAIN_SUFFIX);
        printf(fileUrl);
        xQueueSend(talking_clock_queue, fileUrl, portMAX_DELAY);
    }
}