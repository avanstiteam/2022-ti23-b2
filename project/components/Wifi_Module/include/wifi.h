#ifndef WIFI_H
#define WIFI_H


#define WIFI_CONNECTED 0
#define WIFI_NOT_CONNECTED 1
#define WIFI_FAILED 2

    void init_wifi(void *pvParameters);
    int get_wifi_state();
#endif    