#ifndef GKAR_MODULE_H
#define GKAR_MODULE_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"



typedef struct lyricsline GKAR_Lyrics_line_t;

struct lyricsline{
    char *text;
    long timestamp; // Timestamp in seconds since start
} ;

typedef QueueHandle_t LyricsHandlingQueue;

LyricsHandlingQueue *GKAR_GetLyricsQueueHandle(void);

void GKAR_Init();

void GKAR_start(char *songName);

void GKAR_Reset();

#endif