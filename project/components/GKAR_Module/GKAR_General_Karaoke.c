#include "GKAR_General_Karaoke.h"

#include "esp_log.h"
#include "KARVIS_ReaderModule.h"
#include "LGM_module.h"
#define LYRICS_QUEUE_SIZE 100

LyricsHandlingQueue lyricsQueue = NULL;

static const char *TAG = "GKAR_MODULE";

LyricsHandlingQueue *GKAR_GetLyricsQueueHandle( void )
{
    return &lyricsQueue;
}

void GKAR_Init()
{
    if (lyricsQueue == NULL)
    {
        lyricsQueue = xQueueCreate(LYRICS_QUEUE_SIZE, sizeof(GKAR_Lyrics_line_t));
    }

    ESP_LOGI(TAG, "Created the GKAR module");

    KARVIS_StartReading();
}


void GKAR_start(char *songName)
{
    xTaskCreate(LGM_Fill_LQueue, "http", 8192, songName, 1, NULL);
}

void GKAR_Reset()
{
    xQueueReset(lyricsQueue);
}