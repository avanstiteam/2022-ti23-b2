#include <stdio.h>
#include "Rotary_encoder_driver.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "esp_spi_flash.h"

#include "qwiic_twist.h"

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#define I2C_MASTER_FREQ_HZ       100000
#define I2C_MASTER_SDA_IO        18
#define I2C_MASTER_SCL_IO        23

#define ROTERY_ENCODER_ADDRESS   0x3f

#define CLK_PIN  2
#define DATA_PIN 7
#define YLED A1

qwiic_twist_t pQwiic_twist;


#define COLOR_COUNT 8
int color_array[COLOR_COUNT][3] = {
     {255,0,0},         //Color: red
     {0, 255, 0},       //Color: green
     {0, 0, 255},       //Color: blue
     {255, 255, 0},     //Color: yellow
     {154, 50, 204},    //Color: purple
     {255, 255, 255},   //Color: white
     {255, 69, 0},      //Color: orange
     {255, 105, 180}    //Color: pink
};

int defaultRoteryEncoderColor; //should be replaced with an array of colors

//Sets up I2C for the rotary encoder
static void i2c_init(void) {

    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;

    i2c_set_timeout(I2C_MASTER_NUM, 20000);
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_LEN, I2C_MASTER_TX_BUF_LEN, 0);
}

/*
* its returns the qwiic_twist_t object to control the roterty encoder
* it's used to use the rotery encoer anywhere.
*/
qwiic_twist_t getRoteryObject(){
    return pQwiic_twist;
}

//test method for printing in the console when the rotary encoder is moved.
void onMoved(int16_t amount) {
    printf("Dit is de print: %d\n", amount);
}

//test method for printing in the console when the rotary encoder is pressed.
void onButtonPressed(void) {
    printf("De ROTERORY ENCODER is ingedrukt \n");
}

/*
* Set's the onButtonClicked callback, that will be called when the rotary encoder button is released.
* onMoved: void (*onButtonClicked)(void), the pointer that will be used as a callback.
*/
void set_encoder_onButtonPressed(void (*onButtonPressed)(void)) {
    pQwiic_twist.onButtonPressed = onButtonPressed;
}

/*
* Set's the onButtonClicked callback, that will be called when the rotary encoder button.
* onMoved: void (*onButtonClicked)(void), the pointer that will be used as a callback.
*/
void set_encoder_onButtonClicked(void (*onButtonClicked)(void)) {
    pQwiic_twist.onButtonClicked = onButtonClicked;
}

/*
* Set's the OnMoved callback, that will be called when the rotary encoder moves.
* onMoved: void (*onMoved)(int16_t), the pointer that will be used as a callback.
*/
void set_encoder_onMoved(void (*onMoved)(int16_t)) {
    pQwiic_twist.onMoved = onMoved;
}

//Set's the qwicc twist config to to the correct values
static void config_qwicc_twist_init(void) {    
    pQwiic_twist.i2c_addr = ROTERY_ENCODER_ADDRESS;
    pQwiic_twist.port = I2C_MASTER_NUM;    
    pQwiic_twist.onMoved = onMoved;
    
    qwiic_twist_init(&pQwiic_twist);    
}

//Responsible for initializing all the necessarry compontents
void init_encoder(void) {    
    i2c_init();
    config_qwicc_twist_init();  
    defaultRoteryEncoderColor = 2; //should be replaced with call to qwiic_twist_get_color
    set_new_rotery_color(defaultRoteryEncoderColor);
    qwiic_twist_start_task(&pQwiic_twist);
}

/**
 * It's used to get the current color of the rotery encoder.
 * because of this methode it's possible to get teh current color anywhere.   
 */
int getCurrentRoteryEncoderColor(){
    return defaultRoteryEncoderColor;
}

/**
 * the methode that the change the color that is send by the paramter.
 * Possible to call this methode and change the color by sending the color in the paramter.
 */
void set_new_rotery_color(int colorIndex){ 

    defaultRoteryEncoderColor = colorIndex;
    
    if (colorIndex > COLOR_COUNT)
    {
        return;
    }
    //use the parameter to change the current color of the rotery encoder
    qwiic_twist_set_color(&pQwiic_twist, color_array[colorIndex][0], color_array[colorIndex][1], color_array[colorIndex][2]); 
}