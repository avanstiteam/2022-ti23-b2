#ifndef ROTERY_MENU_H
#define ROTERY_MENU_H

#include <stdio.h>
#include "qwiic_twist.h"

void init_encoder(void);

/*
* Set's the OnMoved callback, that will be called when the rotary encoder moves.
* onMoved: void (*onMoved)(int16_t), the pointer that will be used as a callback.
*/
void set_encoder_onMoved(void (*onMoved)(int16_t));

/*
* Set's the onButtonClicked callback, that will be called when the rotary encoder button is released.
* onMoved: void (*onButtonClicked)(void), the pointer that will be used as a callback.
*/
void set_encoder_onButtonClicked(void (*onButtonClicked)(void));

/*
* Set's the onButtonClicked callback, that will be called when the rotary encoder button.
* onMoved: void (*onButtonClicked)(void), the pointer that will be used as a callback.
*/
void set_encoder_onButtonPressed(void (*onButtonPressed)(void));
void onButtonPressed(void);
void onMoved(int16_t amount);

/**
 * it's used to send the qwiic_twist_t object to adjust the rotery encoder anywhere. 
 */
qwiic_twist_t getRoteryObject();

/**
 * Sets the new color to the rotery encoder
 */
void set_new_rotery_color(int colorIndex);

/**
 * Get the Current color from Rotery Encoder.
 * Possible to see what the color is in the whole project.
 * @return int 
 */
int getCurrentRoteryEncoderColor();

#endif
