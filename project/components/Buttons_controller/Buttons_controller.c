#include <stdio.h>
#include "Buttons_controller.h"
#include "AbstractMenu.h"
#include "Input_Receive.h"
#include "Input_Send.h"
#include "Input_Receive.h"
#include "Input_Codes.h"
#include "Rotary_encoder_driver.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

static void click(void);
static void onEncoderMoved(int16_t i);
static void released(void);

uint32_t lastTick = 0;

/**
 * @brief Initialises all the neccesary components to make the buttons functional
 * 
 */
void BUTTONS_init( void ){
    init_encoder();
    Input_Init(&abstract_menu_handleKeyInput);
    set_encoder_onButtonClicked(&released);
    set_encoder_onMoved(&onEncoderMoved);
    set_encoder_onButtonPressed(&click);
}

/**
 * @brief handles what happens when the rotary encoder is moved. 
 * This method will be used as a function pointer for the set_encoder_onMoved() method.
 * 
 * @param i the amount that the rotary encoder moved.
 */
static void onEncoderMoved(int16_t i){
    if(i > 0){
        Input_SendCommand(MENU_KEY_RIGHT);
    } else if (i < 0){
        Input_SendCommand(MENU_KEY_LEFT);
    }
}

/**
 * @brief handles what happens when the rotary encoder is clicked. 
 * This method will be used as a function pointer for the set_encoder_onButtonClicked() method.
 * 
 */
static void click(void){
    int currentColor = getCurrentRoteryEncoderColor();

    if(AM_initialised){
        printf("clicked\n");
        if(lastTick == 0){
            lastTick = xTaskGetTickCount();
        } 
        
        int milisecFromPressed = (xTaskGetTickCount() - lastTick) * 10;

        if(milisecFromPressed >= 300){
            
            set_new_rotery_color(0);
            vTaskDelay(50);
            set_new_rotery_color(currentColor);
        }
    }
}

/**
 * @brief handles what happens when the rotary encoder is released. 
 * This method will be used as a function pointer for the set_encoder_onButtonPressed() method.
 * 
 */
static void released(void){
    printf("released\n");

    int milisecFromPressed = (xTaskGetTickCount() - lastTick) * 10;
    printf("Milisec: %d\n", milisecFromPressed);

    if(milisecFromPressed >= 300 && milisecFromPressed < 5000){
        Input_SendCommand(MENU_KEY_BACK);
    } else {
        Input_SendCommand(MENU_KEY_SELECT);
    }
    
    lastTick = 0;
    
}


