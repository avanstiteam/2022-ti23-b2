#include "SPI_SSEG_Driver.h"

#include <stdio.h>
#include "GPIO_Extender.h"

#define DIN 0
#define CS 1
#define CLK 2

#define INTENSITY_REG 0x0A
#define SCANLIMIT_REG 0x0B
#define SHUTDOWN_REG 0x0C
#define DISPLAYTEST_REG 0x0F
#define DECODE_REG 0x09

#define INTENSITY_VAL 0x0D
#define DASH_VAL 0b00000001


#define BIT_AT_I(i) ((data & (1 << i)) > 0)

static void clockSendByte(char data)
{
    // Go over all the bits in data
    for (int i = 7; i >= 0; i--)
    {
        GPIO_Extender_write_register(MCP23017_GPIO, GPIOA, BIT_AT_I(i) << DIN);     // Write the bit at index i of data
        GPIO_Extender_write_register(MCP23017_GPIO, GPIOA, 1 << CLK);               // Write clock high
        vTaskDelay(1 / portTICK_PERIOD_MS);                                         // Small delay
        GPIO_Extender_write_register(MCP23017_GPIO, GPIOA, 0 << CLK);               // Write clock low
        vTaskDelay(1 / portTICK_PERIOD_MS);                                         // Small delay
    }
}

static void clockSendRegister(char reg, char data)
{
    GPIO_Extender_write_register(MCP23017_GPIO, GPIOA, (0 << CS));      // Writing the CS to low before starting the read
    clockSendByte(reg);                                                 // Writing the register to write to
    clockSendByte(data);                                                // Writing the data
    GPIO_Extender_write_register(MCP23017_GPIO, GPIOA, (1 << CS));      // Writing the CS to high to finish the write
}

void SPI_SSEG_init()  
{
    GPIO_Extender_write_register(MCP23017_GPIO, GPIOA, (1 << CS));      // Writing the CS to high before starting
    clockSendRegister(SHUTDOWN_REG, 0x00);                              // turn off display
    clockSendRegister(DISPLAYTEST_REG, 0x00);                           // turn off test mode
    clockSendRegister(INTENSITY_REG, INTENSITY_VAL);                    // Setting the intensity to the defined value
}

void SPI_SSEG_Display_time(char timeString[]) // Format of the string should be: HH:MM
{
    clockSendRegister(SHUTDOWN_REG, 0x00);          // Turning off the display
    clockSendRegister(SCANLIMIT_REG, 0x04);         // Writing 5 lights
    clockSendRegister(DECODE_REG, 0b11011011);      // number the dashes not lights should be decode so 0b1101-1011

    // Going over all numbers backwards
    
    clockSendRegister(1, timeString[4]);
    clockSendRegister(2, timeString[3]);

    clockSendRegister(3, DASH_VAL);

    clockSendRegister(4, timeString[1]);
    clockSendRegister(5, timeString[0]);

    clockSendRegister(SHUTDOWN_REG, 0x01);          // Turning the display back on
}