#include "Kar_Time.h"
#include "esp_log.h"
#include "Audio_Handler.h"
#include "audio_element.h"
#include "freertos/FreeRTOS.h"
#include "stdint.h"

static const char *TAG = "KAR_TIME";

void Kar_Time_Task(void *pvParameters);
void fileTimeUpdate(audio_element_info_t *info);

extern audio_element_handle_t wav_decoder;

void Kar_Time_init()
{
    esp_log_level_set(TAG, ESP_LOG_INFO);

    ESP_LOGI(TAG, "Initializing kar_time");

    xTaskCreate(Kar_Time_Task, "Kar_Time_Task", 2048, NULL, 1, NULL);
}

void Kar_Time_Task(void *pvParameters)
{
    audio_element_info_t *music_info;
    audio_element_info_t test = AUDIO_ELEMENT_INFO_DEFAULT();
    music_info = &test;
    for (;;)
    {
        vTaskDelay(100 / portTICK_RATE_MS);
        audio_element_getinfo(wav_decoder, music_info);
        if (music_info != NULL)
        {
            fileTimeUpdate(music_info);
        }
    }
}


int lastMillisElapsed = 0;
void fileTimeUpdate(audio_element_info_t *info)
{
    int64_t bytesRead = info->byte_pos;
    int bytesPerSecond = info->bps;

    if (bytesRead == 0 || bytesPerSecond == 0)
    {
        return;
    }

    double secondsElapsed = bytesRead / (bytesPerSecond / 8.0);
    int milliseconds = secondsElapsed * 1000;

    if (lastMillisElapsed == milliseconds)
    {
        return;
    }
    
    ESP_LOGI(TAG, "fileTimeUpdate byte_pos: %lld total_bytes: %lld bps: %d seconds passed: %f", info->byte_pos, info->total_bytes, info->bps, secondsElapsed);

    lastMillisElapsed = milliseconds;

    return;
}

int Kar_Time_get_millis(void)
{
    ESP_LOGI(TAG, "Gettig milliseconds");
    int returnValue = lastMillisElapsed;
    ESP_LOGI(TAG, "returining milliseconds");
    return (returnValue);
}
