
#ifndef KAR_TIME
#define KAR_TIME

#include "audio_element.h"

void Kar_Time_init(void);
int Kar_Time_get_millis(void);

#endif
