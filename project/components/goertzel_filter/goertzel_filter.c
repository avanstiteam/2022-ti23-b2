/**
 * Goertzel filter
 *
 * Initial implementation by P.S.M. Goossens
 * Based on various sources, such as
 * https://www.embedded.com/the-goertzel-algorithm/
 * https://github.com/jacobrosenthal/Goertzel
 * https://www.st.com/content/ccc/resource/technical/document/design_tip/group0/20/06/95/0b/c3/8d/4a/7b/DM00446805/files/DM00446805.pdf/jcr:content/translations/en.DM00446805.pdf
 *
 * Adapted by Hans van der Linden.
 *
 * Hans van der Linden version adapted by Stijn van Tilburg
 */
#include <math.h>

#include "goertzel_filter.h"
#include "freertos/FreeRTOS.h"


/**
 * Setup the filter using the provided config data to prepare for processing
 */
esp_err_t GOERTZEL_FILTER_setup(goertzel_filter_data_t *dataArray, goertzel_filter_cfg_t *configArray, int amountOfFilters)
{

    for (int i = 0; i < amountOfFilters; i++)
    {
        goertzel_filter_data_t *data = &dataArray[i];
        goertzel_filter_cfg_t *config = &configArray[i];
        // Calculate constants for the Goertzel filter
        float numSamples = (float)config->buffer_length;
        data->buffer_length = config->buffer_length;
        int k = (int)(0.5f + ((numSamples * config->target_freq) / config->sample_rate));
        float omega = (2.0f * M_PI * k) / numSamples;
        data->coefficient = 2.0f * cosf(omega);
    }
    return ESP_OK;
}

/**
 * Clear the filter data to prepare for processing of samples
 */
esp_err_t GOERTZEL_FILTER_clear(goertzel_filter_data_t *dataArray, int amountOfFilters)
{
    for (int i = 0; i < amountOfFilters; i++)
    {
        goertzel_filter_data_t *data = &dataArray[i];
        data->q0 = 0.0f;
        data->q1 = 0.0f;
        data->q2 = 0.0f;
        data->sample_counter = 0; // Start a new block of samples
        data->magnitude = 0.0f;   // Start off with a magnitude of 0
        data->updated = false; // No new magnitude yet
        data->detected = false; // frequency can't be found before calculations are done
    }

    return ESP_OK;
}

/**
 * Checks if the target frequencie was found with processed data
 */
void check_for_frequencie(goertzel_filter_data_t *filter, float detectionThreshold)
{
    // Magnitude calculation
    filter->magnitude = sqrtf(filter->q1 * filter->q1 + filter->q2 * filter->q2 - filter->q1 * filter->q2 * filter->coefficient);
    filter->updated = true;     // New magnitude value is available
    filter->sample_counter = 0; // Start a new block of samples

    // Reset the filter data
    filter->q0 = 0.0f;
    filter->q1 = 0.0f;
    filter->q2 = 0.0f;

    //calculates if the frequency set to the filter was found
    float logMagnitude = 10.0f * log10f(filter->magnitude);
    if (logMagnitude > detectionThreshold)
    {
        filter->detected = true;
    }
}

/**
 * proccesses the data of all of the filters 
 */
esp_err_t GOERTZEL_FILTER_process(goertzel_filter_data_t *filters, int amountOfFilters, int16_t *samples, int numSamples)
{
    for (int i = 0; i < amountOfFilters; i++)
    {
        goertzel_filter_data_t *filter = &filters[i];
        for (int s = 0; s < numSamples; s++)
        {
            // Process one sample
            float sample = (float)samples[s];
            filter->q0 = sample + filter->coefficient * filter->q1 - filter->q2; // Goertzel filter equation
            filter->q2 = filter->q1;
            filter->q1 = filter->q0;
            filter->sample_counter += 1;
        }
         
    }

    return ESP_OK;
}

/**
 * checks if only a singular target frequency was found and returns that number.
 * returns: if a singular frequency was found returns that number otherwise it returns -1
 */
int GOERTZEL_FILTER_check_target_frequencies(goertzel_filter_data_t *filters, int amountOfFilters, float detectionThreshold)
{
    bool FrequencieDetected = false;
    int detectedFrequency = -1;
    goertzel_filter_data_t *filter;
    for (int i = 0; i < amountOfFilters; i++)
    {

        filter = &filters[i];
        check_for_frequencie(filter, detectionThreshold);
        //If another frequency was already detected and this filter also matches a frequency returns -1
        // because multiple frequencies were found
        if (FrequencieDetected && filter->detected)
        {
            return -1; // -1 is the error code
        }

        //if a frequency was detected remember the number of the detected frequency
        if (filter->detected)
        {
            FrequencieDetected = filter->detected;
            detectedFrequency = i;
        }
    }

    return detectedFrequency;
}
