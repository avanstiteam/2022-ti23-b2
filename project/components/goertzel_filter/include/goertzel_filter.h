/**
 * Goertzel filter
 * 
 * Initial implementation by P.S.M. Goossens
 * Based on various sources, such as
 * https://www.embedded.com/the-goertzel-algorithm/
 * https://github.com/jacobrosenthal/Goertzel
 * https://www.st.com/content/ccc/resource/technical/document/design_tip/group0/20/06/95/0b/c3/8d/4a/7b/DM00446805/files/DM00446805.pdf/jcr:content/translations/en.DM00446805.pdf
 * 
 * Adapted by Hans van der Linden
 * 
 * Hans van der Linden version adapted by Stijn van Tilburg
 */
#ifndef GOERTZEL_FILTER_H
#define GOERTZEL_FILTER_H

#include <stdbool.h>
#include "esp_err.h"

struct goertzel_filt_dat_t;

/**
 * @brief Structure containing Goertzel filter configuration data
 */
typedef struct goertzel_filt_conf_t {
    int sample_rate;        // Number of samples per second [Hz]
    int target_freq;        // Target frequency to detect [Hz]
    int buffer_length;      // Number of samples to process [N]
    //float scaling_factor;   // Used in alternative magnitude calculation
} goertzel_filter_cfg_t;

/**
 * @brief Structure containing Goertzel filter data for processing
 */
typedef struct goertzel_filt_dat_t {
    int buffer_length;      // Number of samples to process [N]
    int sample_counter;     // Number of samples handled
    //float scaling_factor;   // Used in alternative magnitude calculation
    //float omega;
    float coefficient;      // Precalculated coefficient
    float q0;
    float q1;
    float q2;
    float magnitude;        // Calculated magnitude value
    bool detected;
    bool updated;           // True whenever new magnitude is calculated
} goertzel_filter_data_t;

/**
 * Setup the filter using the provided config data to prepare for processing
 */
esp_err_t GOERTZEL_FILTER_setup(goertzel_filter_data_t *data, goertzel_filter_cfg_t *config, int amountOfFilters);

/**
 * Clear the filter data to prepare for processing of samples
 */
esp_err_t GOERTZEL_FILTER_clear(goertzel_filter_data_t *data, int amountOfFilters);

/**
 * proccesses the data of all of the filters 
 */
esp_err_t GOERTZEL_FILTER_process(goertzel_filter_data_t *filters, int amountOfFilters, int16_t *samples, int numSamples);

/**
 * checks if only a singular target frequency was found and returns that number.
 * returns: if a singular frequency was found returns that number otherwise it returns -1
 */
int GOERTZEL_FILTER_check_target_frequencies(goertzel_filter_data_t *filters, int amountOfFilters, float detectionThreshold);

#endif