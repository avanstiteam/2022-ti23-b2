#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "AbstractMenu.h"

extern MENU_SCREEN_ITEM radioMenuScreen;
extern MENU_SCREEN_ITEM timeMenuScreen;
extern MENU_SCREEN_ITEM karaokeMenuScreen;
extern MENU_SCREEN_ITEM roteryMenuScreen;
extern MENU_SCREEN_ITEM setAlarm;

#endif