#include "MainMenu.h"
#include "menu_strings.h"
#include "LCDDriver.h"
#include "RadioMenu.h"
#include "KaraokeMenu.h"
#include "custom_icon.h"
#include "RoteryColorPickerMenu.h"
#include "SetUpAlarmMenu.h"

#include "time_manager.h"
#include "audio_time_module.h"
#include "esp_log.h"

//-- RADIO MENU --//

// Onentry and onExit functions
static void radio_onEntry(void);

// Radio menu shows the text of the radio menu
// The sub-text currently is hardcoded
MENU_SCREEN_ITEM radioMenuScreen = {
    // List of all text
    {
        MENU_RADIO_HEADER,
        NULL,
        "MENU",
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT},
    // Screens to go to on button
    {
        &karaokeMenuScreen, &timeMenuScreen, &radioInfoMenuScreen, NULL},
    // Functions on key
    {
        NULL, NULL, NULL, NULL},

    // On Entry
    radio_onEntry,

    // On Exit
    NULL,

    // Icon
    INDEX_RADIO_ICON
    };

/*
 * When the radio menu is shown, loac the current radio playing
 */
static void radio_onEntry(void)
{
    // TODO: Get the current radio from radio menu
    LCD_Write_string_On_ScreenPos(":zender:", MID_BOTTOM);
}

//-- TIME MENU --//
static void time_onSelect(void);
static void time_onEntry(void);
static void time_onExit(void);

static const char *TAG = "TIME_MENU";

TaskHandle_t time_taskHandle = NULL;

// Time menu shows the time header and the current time
// On select the time should be read TODO:remove hard code
MENU_SCREEN_ITEM timeMenuScreen = {
    // List of all text
    {
        MENU_CLOCK_HEADER,
        NULL,
        "MENU",
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT
        },
    // Screens to go to on button
    { 
        &radioMenuScreen, &setAlarm, NULL, NULL
    },
    // Functions on key
    {
        NULL, NULL, time_onSelect, NULL},

    // On Entry
    time_onEntry,

    // On Exit
    time_onExit,

    // Icon
    INDEX_CLOCK_ICON
    };

static int timeRunning = 0;

/**
 * @brief Method is called when the time updates comes in.
 * It updatest the time on the screen
 *
 * @param time The current time
 */
static void fOnTimeUpdate(time_t now)
{
    ESP_LOGV(TAG, "fOnTimeUpdate called");
    if (timeRunning == 0)
        return;

    char stringTimeBuffer[20];
    struct tm timeStruct;

    localtime_r(&now, &timeStruct);
    strftime(stringTimeBuffer, sizeof(stringTimeBuffer), "%H:%M", &timeStruct);

    LCD_Write_string_On_ScreenPos(stringTimeBuffer, MID_BOTTOM);
}

// Show the current time on the display
static void time_onEntry(void)
{

    timeRunning = 1;
    esp_log_level_set(TAG, ESP_LOG_INFO);

    ESP_LOGI(TAG, "time_onEntry called");
    // Get the time from the time component
    TIME_give_time_update(&fOnTimeUpdate, &time_taskHandle);

    ESP_LOGI(TAG, "time_onEntry exited");
}

// Turns off the timeRunning varable to stop the time sync
static void time_onExit(void)
{
    timeRunning = 0;
    ESP_LOGI(TAG, "time_onExit called");
    vTaskDelete(time_taskHandle);
}

// Calls the method to tell the time from the audio module
static void time_onSelect(void)
{
    TIME_speak_current_time();
}

//-- KARAOKE MENU --//

// Radio menu shows the text of the radio menu
// The sub-text currently is hardcoded
MENU_SCREEN_ITEM karaokeMenuScreen = {
    // List of all text
    {
        MENU_KARAOKE_HEADER,
        MENU_KARAOKE_SUB_HEADER,
        "MENU",
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT},
    // Screens to go to on button
    { 
        &roteryMenuScreen, &radioMenuScreen, &karaokeInfoMenuScreen, NULL
    },
    // Functions on key
    {
        NULL, NULL, NULL, NULL},

    // On Entry
    NULL,

    // On Exit
    NULL,

    // ICON
    INDEX_MUSIC_NOTE_ICON
    };


//-- RADIO MENU --//

// Radio menu shows the text of the radio menu
// The sub-text currently is hardcoded
MENU_SCREEN_ITEM roteryMenuScreen = {
    // List of all text
    {
        MENU_ROTERY_HEADER,
        MENU_ROTERY_SUB_HEADER,
        "MENU",
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT
    },
    // Screens to go to on button
    { 
        &setAlarm, &karaokeMenuScreen, &roteryColorPickerMenuScreen, NULL
    },
    // Functions on key
    {
        NULL, NULL, NULL, NULL
    },

    // On Entry
    NULL,

    // On Exit
    NULL,
    0
};

//-- ALARM MENU --//
MENU_SCREEN_ITEM setAlarm = {
    // List of all text
    {
        NULL,
        MENU_ALARM_HEADER,
        "HOOFDMENU",
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT
    },
    // Screens to go to on button
    { 
        &timeMenuScreen, &roteryMenuScreen, &setUpAlarmMenu, NULL
    },
    // Functions on key
    {
        NULL, NULL, NULL, NULL
    },

    // On Entry
    NULL,

    // On Exit
    NULL
};

