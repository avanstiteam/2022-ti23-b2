#include "MainMenu.h"
#include "LCDDriver.h"
#include "menu_strings.h"
#include "Audio_Handler.h"
#include "KaraokeMenu.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "LGM_module.h"

//-- RADIO INFO MENU --//

static void karaoke_onEntry( void );
static void karaoke_onExit( void );
static void show_karaoke_left(void);
static void show_karaoke_right(void);
static void set_karaoke_song(void);

typedef struct
{
    char *name;
    char *url;
    char *songAndArtist;
    char *artist;
} karaoke_number_t;

/* All radios available on this device */
karaoke_number_t songs[] = {
        {"Hotel California","file://sdcard/hotel.wav", "GOLDEN_EARRING-TWILIGHT_ZONE", "Eagles"},
        {"Twilight zone", "file://sdcard/hotel.wav", "GOLDEN_EARRING-TWILIGHT_ZONE", "Golden Earring"}
};

int currentSongIndex = 0;

karaoke_number_t currentSong;



// Radio menu shows the text of the radio menu
// The sub-text currently is hardcoded
MENU_SCREEN_ITEM karaokeInfoMenuScreen = {
    // List of all text
    {
        KARAOKE_INFO_MENU_HEADER,
        NULL,
        NULL,
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT
    },
    // Screens to go to on button
    { 
        NULL, NULL, NULL, &karaokeMenuScreen
    },
    // Functions on key
    {
        &show_karaoke_left, &show_karaoke_right, &set_karaoke_song, NULL
    },

    // On Entry
    karaoke_onEntry,

    // On Exit
    karaoke_onExit,
    0

};

/*
 * When the radio menu is shown, loac the current radio playing
 */
static void karaoke_onEntry( void ) {

    currentSongIndex = 0;
    currentSong = songs[currentSongIndex];

    LCD_Write_string_On_ScreenPos(songs[currentSongIndex].name, MID_BOTTOM);
}

static void karaoke_onExit( void ) {

    audio_pause();
    GKAR_Reset();

}

static void set_karaoke_song(void)
{
    currentSong = songs[currentSongIndex];

    LCD_Clear_Screen();
    LCD_Write_String_Middle(currentSong.name, 1);
    LCD_Write_String_Middle(currentSong.artist, 2);
    GKAR_start("GOLDEN_EARRING-TWILIGHT_ZONE");

    xQueueSend(talking_clock_queue, currentSong.url, portMAX_DELAY);
    
}

static void show_karaoke_left(void)
{
    currentSongIndex = (currentSongIndex + 1) % (sizeof(songs) / sizeof(songs[0]));
    LCD_Clear_Row(2);
    LCD_Write_string_On_ScreenPos(songs[currentSongIndex].name, MID_BOTTOM);
}

static void show_karaoke_right(void)
{
    if (currentSongIndex - 1 < 0)
    {
        currentSongIndex = (sizeof(songs) / sizeof(songs[0])) - 1;
    }
    else
    {
        currentSongIndex--;
    }
    LCD_Clear_Row(2);
    LCD_Write_string_On_ScreenPos(songs[currentSongIndex].name, MID_BOTTOM);
}

   
    
