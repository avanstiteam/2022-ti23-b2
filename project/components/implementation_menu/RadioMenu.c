#include "MainMenu.h"
#include "LCDDriver.h"
#include "menu_strings.h"
#include "Audio_Handler.h"

//-- RADIO INFO MENU --//

/* Struct for savind radio data */
typedef struct
{
    char *name;
    char *url;
} radio_info_t;

/* All radios available on this device */
radio_info_t radios[] = {
        {"NPO3 FM", "http://icecast.omroep.nl/3fm-bb-mp3"},
        {"QMusic", "http://icecast-qmusicnl-cdp.triple-it.nl/Qmusic_nl_live.mp3"},
        {"Skyradio", "http://19993.live.streamtheworld.com/SKYRADIO.mp3"},
        {"538", "http://playerservices.streamtheworld.com/api/livestream-redirect/RADIO538.mp3"},
        {"SLAM!", "http://stream.radiocorp.nl/web14_mp3"},
        {"Radio 10", "http://19993.live.streamtheworld.com/RADIO10.mp3"},
        {"Groot Nieuws Radio","http://grootnieuwsradio.streampartner.nl:8000/live"}};

int currentRadioIndex;

radio_info_t currentRadio;

// Onentry and onExit functions
static void radio_onEntry(void);
static void radio_onExit(void);
static void show_radio_left(void);
static void show_radio_right(void);
static void set_current_radio(void);

// Radio menu shows the text of the radio menu
// The sub-text currently is hardcoded
MENU_SCREEN_ITEM radioInfoMenuScreen = {
    // List of all text
    {
        RADIO_INFO_MENU_HEADER,
        NULL,
        NULL,
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT},
    // Screens to go to on button
    {
        NULL, NULL, NULL, &radioMenuScreen},
    // Functions on key
    {
        &show_radio_left, &show_radio_right, &set_current_radio, NULL},

    // On Entry
    radio_onEntry,

    // On Exit
    radio_onExit,
    0
};

/*
 * When the radio menu is shown, loac the current radio playing
 */
static void radio_onEntry(void)
{
    // Audio_Init();
    currentRadioIndex = 0;
    currentRadio = radios[currentRadioIndex];

    LCD_Write_string_On_ScreenPos(radios[currentRadioIndex].name, MID_BOTTOM);

    //audio_play_http(currentRadio.url);
}

static void radio_onExit(void)
{
    currentRadioIndex = 0;
    currentRadio = radios[currentRadioIndex];
    // Audio_DeInit();
    //Audio_StopPipeline();
    audio_pause();

    // Call the audiostream to stop
}

static void set_current_radio(void)
{
    currentRadio = radios[currentRadioIndex];

    audio_play_http(currentRadio.url);
    // TODO call the audiostream to this radio info
}

static void show_radio_left(void)
{
    currentRadioIndex = (currentRadioIndex + 1) % (sizeof(radios) / sizeof(radios[0]));
    LCD_Clear_Row(2);
    LCD_Write_string_On_ScreenPos(radios[currentRadioIndex].name, MID_BOTTOM);
}

static void show_radio_right(void)
{
    if (currentRadioIndex - 1 < 0)
    {
        currentRadioIndex = (sizeof(radios) / sizeof(radios[0])) - 1;
    }
    else
    {
        currentRadioIndex--;
    }
    LCD_Clear_Row(2);
    LCD_Write_string_On_ScreenPos(radios[currentRadioIndex].name, MID_BOTTOM);
}