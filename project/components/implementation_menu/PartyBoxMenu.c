#include "PartyBoxMenu.h"
#include "menu_strings.h"
#include "MainMenu.h"

void boot_onEntry(void);

// First menu item shows boot up signal
// Shows boot text
// After set delay will move on to next screen
MENU_SCREEN_ITEM firstMenuScreen = {
    // List of all text
    {
        " ",
        BOOT_HEADER_1,
        " ",
        " "
    },
    // Screens to go to on button
    { 
        NULL, NULL, NULL, NULL
    },
    // Functions on key
    {
        NULL, NULL, NULL, NULL
    },

    // On Entry
    boot_onEntry,

    // On Exit
    NULL,
    0
};

// Booting menu waits for defined time, then moves on
void boot_onEntry(void) {
    abstract_menu_switchScreen(&radioMenuScreen);
}
