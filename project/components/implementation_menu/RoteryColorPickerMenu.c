#include "MainMenu.h"
#include "LCDDriver.h"
#include "menu_strings.h"

#include "RoteryColorPickerMenu.h"
#include "Rotary_encoder_driver.h"

typedef struct
{
    int id;
    char* color;
} color_t;


color_t colors[] = {
    {0, "red"},
    {1, "green"},
    {2, "blue"},
    {3, "yellow"},
    {4, "purple"},
    {5, "white"},
    {6, "orange"},
    {7, "pink"}
};


int currentColorIndex = 0;
color_t currentColor;

static void roteryEncode_color_onEntry(void);
static void show_previous_color(void);
static void show_next_color(void);
static void set_current_color(void);

MENU_SCREEN_ITEM roteryColorPickerMenuScreen = {
    // List of all text
    {
        "Choose A New Color",
        NULL,
        NULL,
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT
    },
    // Screens to go to on button
    { 
        NULL, NULL, NULL, &roteryMenuScreen
    },
    // Functions on key
    {
         &show_previous_color, &show_next_color, &set_current_color, NULL
    },
    // On Entry
    roteryEncode_color_onEntry,

    // On Exit
    NULL,
    0
};


/*
 * If the roteryEncoderColorChange menu is entered, then the current color will be visible in the menu.
 * The methode is used to enter the menu to change the color. And that the current color will be visible. 
 */
static void roteryEncode_color_onEntry(void)
{
    currentColor.id = getCurrentRoteryEncoderColor();
    currentColor = colors[currentColor.id];

    LCD_Write_string_On_ScreenPos(currentColor.color, MID_BOTTOM);    
}

/*
 * It will show the previous color on the menu.
 * The methode is used to see what the previous color was.
 */
static void show_previous_color(void)
{
    currentColorIndex = (currentColorIndex + 1) % (sizeof(colors) / sizeof(colors[0]));
    LCD_Clear_Row(2);
    LCD_Write_string_On_ScreenPos(colors[currentColorIndex].color, MID_BOTTOM);
}

/*
 * It will show the next color on the menu.
 * The methode is used to see what the next color was.
 */
static void show_next_color(void)
{
    if (currentColorIndex - 1 < 0) {
        currentColorIndex = (sizeof(colors) / sizeof(colors[0])) - 1;
    } else {
        currentColorIndex--;
    }

    LCD_Clear_Row(2);
    LCD_Write_string_On_ScreenPos(colors[currentColorIndex].color, MID_BOTTOM);
}

/*
 * It will select the color and sets it in the rotery encoder. 
 * The methode is used to change the color.
 */
static void set_current_color(void)
{        
    currentColor = colors[currentColorIndex];
    
    set_new_rotery_color(currentColor.id);
}

