#include "MainMenu.h"
#include "LCDDriver.h"
#include "menu_strings.h"
#include "SetUpAlarmMenu.h"
#include <string.h>
#include "system_time.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "Audio_Handler.h"
#include "TC_Driver.h"


//A struct to save a time(hour and minute)
typedef struct
{
    int hourID;
    int minuteID;
} alarm_clock_t;

//struct used to display the minute on alarm clock menu
typedef struct
{
    int id;
    char* minute;
} minute_t;

//struct used to display the hour on alarm clock menu
typedef struct
{
    int id;
    char* hour;
} hour_t;

//All the minutes that will be displayed on the alarm clock menu
minute_t minute[] = { 
    {0, "00"},    {1, "01"},   {2, "02"},   {3, "03"},    {4, "04"},    {5, "05"},
    {6, "06"},    {7, "07"},   {8, "08"},   {9, "09"},    {10, "10"},   {11, "11"},
    {12, "12"},   {13, "13"},  {14, "14"},  {15, "15"},   {16, "16"},   {17, "17"},
    {18, "18"},   {19, "19"},  {20, "20"},  {21, "21"},   {22, "22"},   {23, "23"},
    {23, "23"},   {24, "24"},  {25, "25"},  {26, "26"},   {27, "27"},   {28, "28"},  
    {29, "29"},   {30, "30"},  {31, "31"},  {32, "32"},   {33, "33"},   {34, "34"},
    {35, "35"},   {36, "36"},  {37, "37"},  {38, "38"},   {39, "39"},   {40, "40"},
    {41, "41"},   {42, "43"},  {43, "43"},  {44, "44"},   {45, "45"},   {46, "46"},
    {47, "47"},   {48, "48"},  {49, "49"},  {50, "50"},   {51, "51"},   {52, "52"},
    {53, "53"},   {54, "54"},  {55, "55"},  {56, "56"},   {57, "57"},   {58, "58"},
    {59, "59"}
};

//All the hours that will be displayed on the alarm clock menu
hour_t hour[] = {
    {0, "00"},   {1, "01"},   {2, "02"},   {3, "03"},
    {4, "04"},   {5, "05"},   {6, "06"},   {7, "07"},      
    {8, "08"},   {9, "09"},   {10, "10"},  {11, "11"},
    {12, "12"},  {13, "13"},  {14, "14"},  {15, "15"},
    {16, "16"},  {17, "17"},  {18, "18"},  {19, "19"},
    {20, "20"},  {21, "21"},  {22, "22"},  {23, "23"}
};

//indicator for the array hour index
int currentHourIndex = 0;
//indicator for the array minute index
int currentMinuteIndex = 0;
//indicator for the left or right time on the alarm clock menu
int clickCounterLR = 1;
//indicator for the position on the alarm clock menu
int clickCounterPos = 2;

//A struct that save the choosed time for the alarm clock
alarm_clock_t choosedTime;
//A struct that save the current time
alarm_clock_t currentTime;

char displayHour[3];
char displayMinute[3];

static void setup_alarm_onEntry(void);
static void show_previous_time(void);
static void show_next_time(void);
static void set_alarm_clock_time(void);
static void check_alarm_time(void);
void getCurrentTime(void);
static  void display_time_onEntry(void);

MENU_SCREEN_ITEM setUpAlarmMenu = {
    // List of all text
    {
        NULL,
        NULL,
        NULL,
        NULL,
        ARROW_LEFT,
        ARROW_RIGHT
    },
    // Screens to go to on button
    { 
        NULL, NULL, NULL, &setAlarm
    },
    // Functions on key
    {
         &show_previous_time, &show_next_time, &set_alarm_clock_time, NULL
    },
    // On Entry
    setup_alarm_onEntry,

    // On Exit
    NULL
};

//It retrieves the current time, and fill the struct named: 'currentTime' with the current time
void getCurrentTime(){
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );    
    //set the hour in currentTime
    currentTime.hourID = timeinfo->tm_hour;
    //set the minute in currentTime
    currentTime.minuteID = timeinfo->tm_min;
}

//This methode will be call when the alarm clock menu is visited.
static void setup_alarm_onEntry(void)
{        
    //Updates the current time
    getCurrentTime();

    LCD_Write_String_At("Choose Time", 0 , 3);
    LCD_Write_String_At("--", 1, 7);    

    display_time_onEntry();    
}

static void display_time_onEntry(){
    //converts the time(int) to a char-array
    sprintf(displayHour, "%d",currentTime.hourID );
    sprintf(displayMinute, "%d", currentTime.minuteID );

    LCD_Write_String_At(displayHour, 2, 7);
    LCD_Write_String_At(":", 2, 9);
    LCD_Write_String_At(displayMinute, 2, 10);
    LCD_Write_String_At("Save", 2, 14);
}

//Shows the previous time on the alarm clock menu
static void show_previous_time(void)
{    
    if(clickCounterLR == 1){   
    //It will show the previous HOUR on the alarm clock menu      
        currentHourIndex = (currentHourIndex + 1) % (sizeof(hour) / sizeof(hour[0]));        
        
        //it will save the hour that is choosed for the alarm clock
        choosedTime.hourID = currentHourIndex;
        
        LCD_Write_String_At(hour[currentHourIndex].hour, 2, 7);        

    }else if(clickCounterLR == 2){        
    //It will show the previous MINUTE on the alarm clock menu
        currentMinuteIndex = (currentMinuteIndex + 1) % (sizeof(minute) / sizeof(minute[0]));     
        
        //it will save the minute that is choosed for the alarm clock
        choosedTime.minuteID = currentMinuteIndex;

        LCD_Write_String_At(minute[currentMinuteIndex].minute, 2, 10);            
    }
}

//Shows the next time on the alarm clock menu
static void show_next_time(void)
{
    if(clickCounterLR == 1){
    //It will show the next HOUR on the alarm clock menu      
        if (currentHourIndex - 1 < 0) {
            currentHourIndex = (sizeof(hour) / sizeof(hour[0])) - 1;
        } else {
            currentHourIndex--;
        }

        //it will save the JOUR that is choosed for the alarm clock
        choosedTime.hourID = currentHourIndex;

        LCD_Write_String_At(hour[currentHourIndex].hour, 2, 7);        

    }else if(clickCounterLR == 2){
    //It will show the next MINUTE on the alarm clock menu      
        if (currentMinuteIndex - 1 < 0) {
            currentMinuteIndex = (sizeof(minute) / sizeof(minute[0])) - 1;
        } else {
            currentMinuteIndex--;
        }         

        //it will save the MINUTE that is choosed for the alarm clock
        choosedTime.minuteID = currentMinuteIndex;

        LCD_Write_String_At(minute[currentMinuteIndex].minute, 2, 10);
    }
}
 
//Starts a task when the alarm is set
void start_alarm(){
    xTaskCreate(check_alarm_time, "check alarm time!", 4096, NULL, 1, NULL);
}

//This is the methode that will be triggerd if the task is started
void check_alarm_time(void){

    printf("Wekker gezet om %d : %d \n", choosedTime.hourID, choosedTime.minuteID);

    //loops every 900ms, it retrieves then the current time(hour and minute)
    //and it checks if the current time is equal to the alarm clock time
    //if that is the case all audio will be paused and the alarm clock song will ring for 30 seconds
    //after that it will delete the task
    for (;;){
        getCurrentTime();

        if(choosedTime.hourID == currentTime.hourID && choosedTime.minuteID == currentTime.minuteID ){
            printf("Alarm clock goes off!!!\n");

            //Pause all the current audio what is running
            audio_pause();

            char alarmClockFileUrl[35];
            //Sets the path to the var alarmClockFileUrl
            sprintf(alarmClockFileUrl, "%s%d%s", MAIN_PATH, 21, MAIN_SUFFIX);
        
            //Plays 15-times the sound as the alarm clock sound
            //(Normally it will be a sound of 15-20 sec long)
            for (int i = 0; i < 15; i++)
            {
                xQueueSend(talking_clock_queue, alarmClockFileUrl, portMAX_DELAY);
            }
        
            printf("Task will be DELETED...\n");
            vTaskDelete(NULL);
        }        
    
       vTaskDelay(900);
    }
}

//this methode called everytime when the rotery encoder is clicked
//it checks on wich position the cursor is and sets the int variables right
static void set_alarm_clock_time(void)
{        
    if(clickCounterPos == 1){           
        LCD_Clear_Row(1);
        LCD_Write_String_At("--", 1, 7);

        clickCounterLR = 1;
        clickCounterPos = 2;

    }else if(clickCounterPos == 2){        
        LCD_Clear_Row(1);
        LCD_Write_String_At("--", 1, 10);

        clickCounterLR = 2;
        clickCounterPos = 3;

    }else if(clickCounterPos == 3){    
        LCD_Clear_Row(1);
        LCD_Write_String_At("----", 1, 14);

        start_alarm();        

        clickCounterLR = 3;
        clickCounterPos = 1;          
    }      
}    