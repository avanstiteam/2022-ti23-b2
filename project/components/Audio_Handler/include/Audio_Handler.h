
#ifndef AUDIO_HANDLER
#define AUDIO_HANDLER
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

typedef enum
{
    AH_STATE_NONE = 0,
    AH_STATE_RUNNING = 1,
    AH_STATE_PAUSED = 2,
    AH_STATE_STOPPED = 3,
} audio_handler_state_t;

/**
 * @brief Method should be called before starting anything audio related functions
 * Inits the functions needed for an audio stream
 *
 */
void Audio_Init(void);

/**
 * @brief This method wil unregister the stream from the pipeline
 * Sets the stream to HTTP, then re-registers the stream
 *
 */
void Audio_UseHttpStream(void);

/**
 * @brief This method wil unregister the stream from the pipeline
 * Sets the stream to use SDCARD, then re-registers the stream
 *
 */
void Audio_UseFileStream(void);

/**
 * @brief Stops the audio stream and closes it
 *
 */
void audio_play_http(char *stream_address);
// void Audio_DeInit(void);
// void Audio_StopPipeline(void);
// int Audio_get_mode();

void audio_pause(void);

/**
 * @brief Start the eventhandles for the audiostream with the given parameters
 *
 * @param pvParameters The parameters for the eventhandler
 */
void Audio_RunningEventHandler(void *pvParameters);

void Audio_Event_Iface_Handler(void *pvParameters);

QueueHandle_t talking_clock_queue;


#endif
