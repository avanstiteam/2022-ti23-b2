#include <stdio.h>
#include "Audio_Handler.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"

#include "nvs_flash.h"
#include "esp_wifi.h"

#include "http_stream.h"
#include "i2s_stream.h"
#include "fatfs_stream.h"
#include "wav_decoder.h"
#include "mp3_decoder.h"

#include "Volume_Send.h"
#include "Volume_Receive.h"

#include "esp_log.h"
#include "periph_touch.h"
#include "sdkconfig.h"
#include "board.h"

static const char *TAG = "AUDIO_HANDLER";

audio_pipeline_handle_t pipeline;
audio_element_handle_t fatfs_stream_reader, http_stream_reader, i2s_stream_writer, wav_decoder, mp3_decoder;
esp_periph_set_handle_t periph_set_handle;
audio_event_iface_handle_t event_iface_handle;
audio_pipeline_cfg_t pipeline_cfg_glb;
audio_board_handle_t board_handle;
audio_handler_state_t current_state;
int player_volume;

// Audi is running
// bool audio_pipeline_running = false;
// bool audio_pipeline_paused = false;

#define USING_AUDIO_NO_STREAM 0
#define USING_AUDIO_HTTP_STREAM 1
#define USING_AUDIO_FILE_STREAM 2

#define FATFS_TAG "FATFS"
#define HTTP_TAG "HTTP"

#define WAV_TAG "WAV"
#define MP3_TAG "MP3"

#define I2S_TAG "I2S"


int volume_queue_init = 0;
int audio_type = USING_AUDIO_NO_STREAM;
char *url;

// Methods
void Audio_StartPipeline(void);
void audio_relink_elements(void);
void Audio_UseOtherStream(int new_audio_type);
// void Audio_StopPipeline(void);
void audio_listener(void *pvParameters);
void Audio_EventIfaceHandler(void *pvParameters);

static void audio_reset_pipeline(void);
static void audio_pipeline_stream_restart(void);
void audio_music_info(audio_element_info_t *music_info);
void audio_play_file(char url[]);

void init_i2s_writer();
void init_http_stream();
void init_wav_decoder();
void init_mp3_decoder();

void Audio_DeInit(void);

void Sdcard_Init(void);

/**
 * @brief Method should be called before starting anything audio related functions
 * Inits the functions needed for an audio stream
 *
 */
void Audio_Init(void)
{
    // Example of linking elements into an audio pipeline -- START
    esp_log_level_set(TAG, ESP_LOG_INFO);

  //  Initialize peripherals management
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    periph_set_handle = esp_periph_set_init(&periph_cfg);

    audio_board_sdcard_init(periph_set_handle, SD_MODE_1_LINE);

    ESP_LOGI(TAG, "[3.1] Initialize keys on board");
    audio_board_key_init(periph_set_handle);

    ESP_LOGI(TAG, "[ 2 ] Start codec chip");
    board_handle = audio_board_init();
    audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);

    player_volume = 50;
    audio_hal_get_volume(board_handle->audio_hal, &player_volume);

    ESP_LOGI(TAG, "[3.0] Create audio pipeline for playback");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline_cfg_glb = pipeline_cfg;

    pipeline = audio_pipeline_init(&pipeline_cfg_glb);
    mem_assert(pipeline);

    ESP_LOGI(TAG, "[3.0.1] Create fatfs stream to read data from sdcard");
    url = NULL;
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    fatfs_cfg.task_core = 0;
    fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);
    audio_element_set_uri(fatfs_stream_reader, url);

    audio_pipeline_register(pipeline, fatfs_stream_reader, FATFS_TAG);

    init_http_stream();

    init_wav_decoder();

    init_mp3_decoder();

    init_i2s_writer();

    current_state = AH_STATE_NONE;
    // audio_reset_pipeline();

    xTaskCreate(Audio_RunningEventHandler, "audio_Running", 4096, NULL, 1, NULL);
    xTaskCreate(Audio_EventIfaceHandler, "audio_Listener", 2048, NULL, 1, NULL);
}

void init_i2s_writer()
{
    ESP_LOGI(TAG, "[3.1] Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_cfg.task_core = 0;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);
    audio_pipeline_register(pipeline, i2s_stream_writer, I2S_TAG);
}

void init_http_stream()
{
    ESP_LOGI(TAG, "[3.0.2] Create http stream to read data");
    http_stream_cfg_t http_cfg = HTTP_STREAM_CFG_DEFAULT();
    http_stream_reader = http_stream_init(&http_cfg);
    audio_pipeline_register(pipeline, http_stream_reader, HTTP_TAG);
}

void init_wav_decoder()
{
    ESP_LOGI(TAG, "[3.2] Create wav decoder to decode wav file");
    wav_decoder_cfg_t wav_cfg = DEFAULT_WAV_DECODER_CONFIG();
    wav_decoder = wav_decoder_init(&wav_cfg);
    audio_pipeline_register(pipeline, wav_decoder, WAV_TAG);

    talking_clock_queue = xQueueCreate(10, (sizeof(char) * 35));
}

void init_mp3_decoder()
{
    ESP_LOGI(TAG, "[3.2] Create mp3 decoder to decode mp3 file");
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    mp3_decoder = mp3_decoder_init(&mp3_cfg);
    audio_pipeline_register(pipeline, mp3_decoder, MP3_TAG);
}

int Audio_get_mode()
{
    return audio_type;
}

void audio_relink_elements(void)
{

    ESP_LOGI(TAG, "[2.4] Register all elements to audio pipeline");
    ESP_LOGI(TAG, "[2.5] Link it together file_stream-->decoder-->i2s_stream-->[codec_chip]");

    // audio_pipeline_unlink(pipeline);

    if (audio_type == USING_AUDIO_HTTP_STREAM)
    {
        const char *link_tag[3] = {HTTP_TAG, MP3_TAG, I2S_TAG};
        audio_pipeline_link(pipeline, link_tag, 3);
    }

    if (audio_type == USING_AUDIO_FILE_STREAM)
    {
        const char *link_tag[3] = {FATFS_TAG, WAV_TAG, I2S_TAG};
        audio_pipeline_link(pipeline, link_tag, 3);
    }
}

void audio_play_http(char *stream_address)
{
    static char *old_url = "";
    if (old_url == stream_address && current_state == AH_STATE_RUNNING)
    {
        ESP_LOGI(TAG, "Can't change to the same url");
        return;
    }
    old_url = stream_address;

    Audio_UseHttpStream();
    audio_play_file(stream_address);
}

void audio_play_file(char url[])
{

    if (audio_type == USING_AUDIO_FILE_STREAM)
    {
        ESP_LOGI(TAG, "SET URI TO FATFS");
        audio_element_set_uri(fatfs_stream_reader, url);
    }
    else if (audio_type == USING_AUDIO_HTTP_STREAM)
    {
        // Audio_StopPipeline();
        ESP_LOGI(TAG, "SET URI TO HTTP");
        audio_element_set_uri(http_stream_reader, url);
    }

    audio_reset_pipeline();

    if (current_state == AH_STATE_PAUSED || current_state == AH_STATE_RUNNING)
    {
        ESP_LOGI(TAG, "RUNNING AGAIN");
        audio_pipeline_run(pipeline);
    }
    else if (current_state == AH_STATE_STOPPED || current_state == AH_STATE_NONE)
    {
        Audio_StartPipeline();
    }
    else
    {
        ESP_LOGI(TAG, "SHIT BROKEN: audio_play_file");
    }
}

static void audio_reset_pipeline(void)
{
    ESP_LOGI(TAG, "RESET RINGBUFFER");
    audio_pipeline_reset_ringbuffer(pipeline);
    ESP_LOGI(TAG, "RESET ELEMENTS");
    audio_pipeline_reset_elements(pipeline);
    ESP_LOGI(TAG, "CHANGE STATE");
    audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
    // Example of using an audio event -- START
}

static void audio_pipeline_stream_restart(void)
{
    ESP_LOGW(TAG, "[ * ] Restart stream");

    audio_relink_elements();

    audio_reset_pipeline();

    // audio_pipeline_run(pipeline);
}

/**
 * @brief This method wil unregister the stream from the pipeline
 * Sets the stream to HTTP, then re-registers the stream
 */
void Audio_UseHttpStream(void)
{
    Audio_UseOtherStream(USING_AUDIO_HTTP_STREAM);
}

/**
 * @brief This method wil unregister the stream from the pipeline
 * Sets the stream to use SDCARD, then re-registers the stream
 *
 */
void Audio_UseFileStream(void)
{
    Audio_UseOtherStream(USING_AUDIO_FILE_STREAM);
}

void Audio_UseOtherStream(int new_audio_type)
{
    if (audio_type == new_audio_type)
    {
        if (new_audio_type == USING_AUDIO_HTTP_STREAM)
        {
            audio_pause();
        }

        ESP_LOGI(TAG, "Already using this audio_type");
        return;
    }

    if (current_state == AH_STATE_RUNNING)
    {
        audio_pause();
    }

    audio_type = new_audio_type;

    audio_relink_elements();
}

void Audio_RunningEventHandler(void *pvParameters)
{
    // Starting the audio for the first time with null
    ESP_LOGI(TAG, "Started Audio_RunningEventHandler");
    audio_element_state_t last_state = -1;
    audio_element_state_t el_state;
    while (1)
    {
        // Small delay to let other threads run
        vTaskDelay(100 / portTICK_PERIOD_MS);
        el_state = audio_element_get_state(i2s_stream_writer);
        if (el_state != last_state)
        {
            ESP_LOGI(TAG, "current state: %d", el_state);
            last_state = el_state;
        }
        if (el_state == AEL_STATE_RUNNING)
        {
            current_state = AH_STATE_RUNNING;
        }

        if (el_state == AEL_STATE_STOPPED)
        {
            // Audio_StopPipeline();
            ESP_LOGE(TAG, "PipeLine Entered AH_STATE_STOPPED state");

            // audio_unregister_elements();
            // audio_relink_elements();
            vTaskDelay(1000 / portTICK_PERIOD_MS);

            audio_pipeline_stream_restart();
            audio_pipeline_resume(pipeline);

            audio_type = USING_AUDIO_NO_STREAM;
            continue;
            // todo: should not happen
        }

        if (el_state == AEL_STATE_FINISHED || el_state == AEL_STATE_PAUSED || el_state == AEL_STATE_INIT || el_state == AEL_STATE_STOPPED)
        {
            char element[30];
            if (uxQueueMessagesWaiting(talking_clock_queue) > 0 &&
                xQueueReceive(talking_clock_queue, element, portMAX_DELAY))
            {
                url = element;
                ESP_LOGI(TAG, "Playing file %s", url);
                Audio_UseFileStream();
                audio_play_file(url);
            }
            else if (el_state != AEL_STATE_INIT)
            {
                // No more samples. Pause for now
                audio_pause();
            }
        }
    }
}

void Audio_EventIfaceHandler(void *pvParameters)
{
    audio_element_info_t *music_info;
    audio_element_info_t test = AUDIO_ELEMENT_INFO_DEFAULT();
    music_info = &test;
    while (1)
    {
        // Small delay to let other threads run
        vTaskDelay(100 / portTICK_PERIOD_MS);

        audio_event_iface_msg_t msg;
        // audio_event_iface_handle_t evt = audio_pipeline_get_event_iface(pipeline);
        esp_err_t ret = audio_event_iface_listen(event_iface_handle, &msg, portMAX_DELAY);

        // ESP_LOGI(TAG, "shitworking");
        if (current_state == AH_STATE_RUNNING && ret != ESP_OK)
        {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && (msg.source == (void *)wav_decoder) && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
        {
            audio_element_getinfo(wav_decoder, music_info);

            ESP_LOGI(TAG, "[ * ] Receive music info from music decoder, sample_rates=%d, bits=%d, ch=%d",
                     music_info->sample_rates, music_info->bits, music_info->channels);

            audio_element_setinfo(i2s_stream_writer, music_info);
            i2s_stream_set_clk(i2s_stream_writer, music_info->sample_rates, music_info->bits, music_info->channels);
            continue;
        }

        /* restart stream when the first pipeline element (http_stream_reader in this case) receives stop event (caused by reading errors) */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)http_stream_reader && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (int)msg.data == AEL_STATUS_ERROR_OPEN)
        {
            audio_pipeline_stream_restart();
            audio_pipeline_run(pipeline);
            continue;
        }

        if (msg.source_type == PERIPH_ID_TOUCH && msg.cmd == PERIPH_TOUCH_TAP)
        {
            ESP_LOGI(TAG, "[ * ] Event interface touch Message received : %d", ((int)msg.data));
            if ((int)msg.data == get_input_volup_id())
            {
                ESP_LOGI(TAG, "[ * ] [Vol+] touch tap event");
                player_volume += 5;
                if (player_volume > 100)
                {
                    player_volume = 100;
                }

                audio_hal_set_volume(board_handle->audio_hal, player_volume);
                if (volume_queue_init == 0)
                {
                    Volume_Init();
                    volume_queue_init = 1;
                }
                Volume_SendCommand(player_volume);
                ESP_LOGI(TAG, "[ * ] Volume set to %d %%", player_volume);
            }
            else if ((int)msg.data == get_input_voldown_id())
            {
                ESP_LOGI(TAG, "[ * ] [Vol-] touch tap event");
                player_volume -= 5;
                if (player_volume < 0)
                {
                    player_volume = 0;
                }

                audio_hal_set_volume(board_handle->audio_hal, player_volume);
                if (volume_queue_init == 0)
                {
                    Volume_Init();
                    volume_queue_init = 1;
                }
                Volume_SendCommand(player_volume);
                ESP_LOGI(TAG, "[ * ] Volume set to %d %%", player_volume);
            }
        }
    }
}


void audio_pause()
{
    if (current_state == AH_STATE_PAUSED)
    {
        return;
    }

    audio_pipeline_pause(pipeline);

    current_state = AH_STATE_PAUSED;
}

/**
 * @brief Stops the audio stream and closes it
 *
 */
// void Audio_DeInit(void)
// {
//     Audio_StopPipeline();
//     audio_pipeline_terminate(pipeline);
//     audio_pipeline_remove_listener(pipeline);

//     esp_periph_set_stop_all(periph_set_handle);
//     audio_event_iface_remove_listener(esp_periph_set_get_event_iface(periph_set_handle), event_iface_handle);

//     /* Make sure audio_pipeline_remove_listener & audio_event_iface_remove_listener are called before destroying event_iface */
//     audio_event_iface_destroy(event_iface_handle);

//     /* Release all resources */
//     audio_pipeline_deinit(pipeline);
//     audio_element_deinit(http_stream_reader);
//     audio_element_deinit(i2s_stream_writer);
//     audio_element_deinit(wav_decoder);
//     esp_periph_set_destroy(periph_set_handle);
// }

void Audio_StartPipeline(void)
{

    audio_pipeline_reset_ringbuffer(pipeline);
    audio_pipeline_reset_elements(pipeline);
    audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
    // Example of using an audio event -- START
    ESP_LOGI(TAG, "[ 4 ] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt_cfg.external_queue_size = 10;
    event_iface_handle = audio_event_iface_init(&evt_cfg);

    ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(pipeline, event_iface_handle);

    ESP_LOGI(TAG, "[4.2] Listening event from peripherals");
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(periph_set_handle), event_iface_handle);

    ESP_LOGI(TAG, "[ 5 ] Start audio_pipeline");
    audio_pipeline_run(pipeline);

    current_state = AH_STATE_RUNNING;
}
