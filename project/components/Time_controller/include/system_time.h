#ifndef TIME_CONTROLLER_H
#define TIMER_CONTROLLER_H

#include "time_manager.h"
#include <time.h>

/*
*  Returns the current time of the system
*/
time_t TIME_Get_Current_Time( void );

/*
* Starts the time from internet using the SNTP protocol
*/
void TIME_start_sntp ( void );

#endif