#ifndef TIME_MANAGER
#define TIME_MANAGER

#include "system_time.h"
#include "SSEG_time_updater.h"
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define TIME_DELAY 1000

/*
 * Given a function pointer and time, this method will call the function pointed to
 * with the correct when the given delay passes.
 * The delay is defined in #TIME_DELAY
 * The function will need to be called again for the next update
 */
void TIME_give_time_update(void (*fonTimeCall)(time_t), TaskHandle_t *taskHandle);

#endif
