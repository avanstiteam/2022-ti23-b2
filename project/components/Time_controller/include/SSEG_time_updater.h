#ifndef SSEG_TIME_UPDATER
#define SSEG_TIME_UPDATER

/**
 * @brief Starts the time updates to the SSEG module
 * 
 */
void TIME_start_SSEG_time();

#endif