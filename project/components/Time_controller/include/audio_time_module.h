#ifndef AUDIO_TIME_MODULE_H
#define AUDIO_TIME_MODULE_H

/**
 * @brief Calling this method will make the clock speak the current
 * time. 
 */
void TIME_speak_current_time( void );

#endif