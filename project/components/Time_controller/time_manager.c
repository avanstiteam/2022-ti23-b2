
#include "esp_system.h"
#include "time_manager.h"
#include "esp_log.h"

static const char *TAG = "TIME_MANAGER";

/*
 *  Task that will call a function pointer as long as running is true
 */
static void time_update_Task(void *parameters)
{

    ESP_LOGI(TAG, "Time_update_Task: started");
    // Getting the functionpointer that was passed
    void (*fonTimeCall)(time_t) = parameters;

    // Delaying for the set time
    for (;;)
    {
        (fonTimeCall)(TIME_Get_Current_Time());

        vTaskDelay(TIME_DELAY / portTICK_RATE_MS);
        // Calling the function pointer
    }


    // Deleting the task
    vTaskDelete(NULL);
}

/*
 * Given a function pointer and time, this method will call the function pointed to
 * with the correct time each time the given delay passes.
 * Returns: Id of the function
 */
void TIME_give_time_update(void (*fonTimeCall)(time_t), TaskHandle_t *taskHandle)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);
    // Starting the task to wait and call the time update
    xTaskCreate(time_update_Task, "Time Function", 2048, (void *)fonTimeCall, 1, taskHandle);

    ESP_LOGI(TAG, "TIME_give_time_update: exited");

    return;
}