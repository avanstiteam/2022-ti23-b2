#include "time_manager.h"

#include "SPI_SSEG_Driver.h"

void onTimeFunction();
void updateTime(time_t time);

void TIME_start_SSEG_time()
{
    // First calling the function with the current time
    onTimeFunction();
}

void onTimeFunction()
{
    TIME_give_time_update(&updateTime,NULL);
}

int lastMinute = -1;

void updateTime(time_t now)
{
    char stringTimeBuffer[20];
    struct tm timeStruct;

    localtime_r(&now, &timeStruct);

    if(lastMinute != timeStruct.tm_min)
    {
        strftime(stringTimeBuffer, sizeof(stringTimeBuffer), "%H:%M", &timeStruct);

        
        SPI_SSEG_Display_time(stringTimeBuffer);

        // Setting last minute to prevent updates
        lastMinute = timeStruct.tm_min;
    }
}
