#include "audio_time_module.h"
#include "time_manager.h"

#include <stdio.h>
#include "TC_Driver.h"

#define TWELFE_HOUR_CLOCK

void TIME_speak_current_time( void )
{
    // Get the current time
    time_t now = TIME_Get_Current_Time();
    struct tm timeStruct;

    localtime_r(&now, &timeStruct); // Move the time to the formatted string

    int hours = 0;
    int minutes = timeStruct.tm_min;

    // Play the current hours
    #ifdef TWELFE_HOUR_CLOCK
        hours = timeStruct.tm_hour % 12;
    # else
        hours = timeStruct.tm_hour;
    #endif 
    
    TC_Speak_Time(hours, minutes);
}

