
#include <string.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "esp_sleep.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"

#include "LCDDriver.h"
#include "esp_system.h"

#include "system_time.h"
#include "esp_sntp.h"   

#define SNTP_TIME_SERVER "pool.ntp.org"

static const char *TAG = "SNTP";

char currentTime[20];
char currentDate[30];

static void obtain_time(void);
static void initialize_sntp(void);


/*
*  Returns the current time of the system
*/
time_t TIME_Get_Current_Time( void )
{
    // Defining the variable to store the current time in
    time_t now;
    struct tm timeInfo;
    time(&now);

    // Settings the time-zone to +1
    setenv("TZ", "CET-1", 1);

    //Request time from the system
    tzset();

    // Getting the time to the variable
    localtime_r(&now, &timeInfo);

    // Return the time
    return now;
}

/*
* Starts the time from internet using the SNTP protocol
*/
void TIME_start_sntp()
{
    time_t now;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);

    // Check if there if the time has been set
    if (timeinfo.tm_year < (2000 - 1900))
    {
        ESP_LOGI(TAG, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
        obtain_time();

    }
}

/*
* Gets the time from the internet using sntp
*/
static void obtain_time(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());

    // Check if wifi is available
    ESP_ERROR_CHECK(example_connect());


    initialize_sntp();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo;
    int retry = 0;

    const int retry_count = 10;
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < retry_count)
    {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }

    // Setting the received time to the system
    time(&now);
    localtime_r(&now, &timeinfo);

    // Printing the time
    ESP_LOGI(TAG, "Time set to unix %ld", TIME_Get_Current_Time());    
}

static void initialize_sntp(void)
{
    ESP_LOGI(TAG, "Initializing Time protocol (SNTP)");

    // Set the mode to polling
    sntp_setoperatingmode(SNTP_OPMODE_POLL);

    // Set the server name
    sntp_setservername(0, SNTP_TIME_SERVER);

    // Callin the init for SNTP
    sntp_init();
}
