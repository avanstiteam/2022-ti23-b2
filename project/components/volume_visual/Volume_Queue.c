#include "Volume_Queue.h"

VolumeQueueHandle volumeQueue = NULL;

VolumeQueueHandle *Volume_GetVolumeQueueHandle(void) {
    return &volumeQueue;
}

VolumeQueueHandle *Volume_Queue_Init(void) {
    if (volumeQueue == NULL)
    {
        volumeQueue = xQueueCreate(VOLUME_QUEUE_SIZE, sizeof(int));
    }

    return &volumeQueue;
}

void Volume_ResetInputQueue(void) {
    xQueueReset(volumeQueue);
}