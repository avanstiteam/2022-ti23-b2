#include "Volume_Receive.h"
#include "Volume_Queue.h"

int Volume_SendCommand(int volume)
{
    VolumeQueueHandle *volumeQueue = Volume_GetVolumeQueueHandle();

    if (xQueueSendToBack(*volumeQueue, &volume, 10) != pdPASS) {
            /* Data could not be sent to the queue even after waiting 10 ticks. */
            return 0;
        }

    return 1;
}
