//
// Created by Robin Luijten on 21/03/2022.
//
#include "Volume_visual.h"
#include "LCDUtil.h"
#include "LCDDriver.h"
#include "custom_icon.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"


TaskHandle_t volumeTaskHandler = NULL;

void Volume_write(int volume){

    int index_sound_col = 6;
    int parsed_volume = volume/10;
    LCD_Write_String_At("           ",0,5);
    LCD_Write_Char_At(INDEX_SOUND_ICON, 0, 5);
    for (int i = 0; i < parsed_volume; i = i+2) {
        LCD_Write_Char_At(INDEX_CONNECTED_ICON_3, 0, index_sound_col++);
    }

    if (volume < 10){
        LCD_Write_String_At("0",0,12);
        char buffer[12];
        sprintf(buffer, "%d",volume);
        LCD_Write_Char_At(buffer[0],0,13);
        LCD_Write_String_At("%",0,14);
    } else if(volume == 100) {
        char buffer[11];
        sprintf(buffer, "%d",volume);
        LCD_Write_Char_At(buffer[0],0,12);
        LCD_Write_Char_At(buffer[1],0,13);
        LCD_Write_Char_At(buffer[2],0,14);
        LCD_Write_String_At("%",0,15);
    }else{
            char buffer[11];
            sprintf(buffer, "%d",volume);
            LCD_Write_Char_At(buffer[0],0,12);
            LCD_Write_Char_At(buffer[1],0,13);
            LCD_Write_String_At("%",0,14);

    }

}


void Volume_remove(){

    LCD_Write_String_At("           ",0,5);

}