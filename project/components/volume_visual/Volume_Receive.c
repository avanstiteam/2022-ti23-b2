#include <stdio.h>
#include "Volume_Receive.h"
#include "Volume_Queue.h"
#include "Volume_visual.h"


void Volume_Handler_Task(void *pvParameters);
void (*Volume_Receive_listener)(int) = NULL;

int Volume_SetVolumeListener(void (*Volume_Receive)(int)) {
    if (Volume_Receive_listener != NULL && Volume_Receive != NULL) {
        return 0;
    }

    Volume_Receive_listener = Volume_Receive;

    return 1;
}

int Volume_Init() {
    VolumeQueueHandle *volumeQueue = Volume_Queue_Init();
    xTaskCreate(&Volume_Handler_Task, "Volume_Handler_Task", 4096, (void *)volumeQueue, 2, NULL);

    return 0;
}

void Volume_Handler_Task(void *pvParameters) {
    VolumeQueueHandle *volumeQueue = (VolumeQueueHandle *)pvParameters;
    int holder;
    int visable = 0;
    for (;;) {
        /* Wait for the maximum period for data to become available on the queue.
        The period will be indefinite if INCLUDE_vTaskSuspend is set to 1 in
        FreeRTOSConfig.h. */
        if (xQueueReceive(*volumeQueue, &holder, ( TickType_t ) (5000/ portTICK_RATE_MS) ) != pdPASS) {
            /* Nothing was received from the queue – even after blocking to wait
            for data to arrive. */
            //printf("nothing in queue to print \n");
            //vTaskDelay(10000 / portTICK_RATE_MS);
            if (visable == 1){
                Volume_remove();
            }

        }
        else {
            Volume_write(holder);
            visable = 1;
        }
        vTaskDelay(10 / portTICK_RATE_MS);
    }
}
