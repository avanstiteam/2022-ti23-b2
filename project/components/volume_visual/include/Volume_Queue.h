#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#pragma once

#define VOLUME_QUEUE_SIZE 30

typedef QueueHandle_t VolumeQueueHandle;

VolumeQueueHandle *Volume_GetVolumeQueueHandle(void);
VolumeQueueHandle *Volume_Queue_Init(void);
void Volume_ResetVolumeQueue(void);