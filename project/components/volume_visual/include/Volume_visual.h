//
// Created by Robin Luijten on 21/03/2022.
//

#ifndef PROJECT_VOLUME_VISUAL_H
#define PROJECT_VOLUME_VISUAL_H


void Volume_write(int volume);
void Volume_remove();
#endif //PROJECT_VOLUME_VISUAL_H
