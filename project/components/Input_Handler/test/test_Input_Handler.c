#include <limits.h>
#include "unity.h"
#include "Input_Codes.h"
#include "Input_Queue.h"
#include "Input_Send.h"

#define INPUT_QUEUE_SIZE 10

TEST_CASE("Testing InputQueue maxSize","[test_Input_Handler]")
{
    Input_Queue_Init();
    Input_ResetInputQueue();

    for (size_t i = 0; i < INPUT_QUEUE_SIZE; i++)
    {
       TEST_ASSERT(Input_SendCommand(MENU_KEY_LEFT));
    }
    TEST_ASSERT_FALSE(Input_SendCommand(MENU_KEY_LEFT));

}