#include "Input_Receive.h"
#include "Input_Queue.h"

int Input_SendCommand(int menu_key)
{
    InputQueueHandle *inputQueue = Input_GetInputQueueHandle();

    if (xQueueSendToBack(*inputQueue, &menu_key, 10) != pdPASS) {
            /* Data could not be sent to the queue even after waiting 10 ticks. */
            return 0;
        }

    return 1;
}
