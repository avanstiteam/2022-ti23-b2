#include <stdio.h>
#include "Input_Receive.h"
#include "Input_Queue.h"


void Input_Handler_Task(void *pvParameters);
void (*Input_Receive_listener)(int) = NULL;

int Input_SetInputListener(void (*Input_Receive)(int)) {
    if (Input_Receive_listener != NULL && Input_Receive != NULL) {
        return 0;
    }

    Input_Receive_listener = Input_Receive;

    return 1;
}

int Input_Init(void (*Input_Receive)(int)) {
    Input_SetInputListener(Input_Receive);
    InputQueueHandle *inputQueue = Input_Queue_Init();
    xTaskCreate(&Input_Handler_Task, "Input_Handler_Task", 4096, (void *)inputQueue, 2, NULL);

    return 0;
}

void Input_Handler_Task(void *pvParameters) {
    InputQueueHandle *inputQueue = (InputQueueHandle *)pvParameters;
    int holder;

    for (;;) {
        /* Wait for the maximum period for data to become available on the queue.
        The period will be indefinite if INCLUDE_vTaskSuspend is set to 1 in
        FreeRTOSConfig.h. */
        if (xQueueReceive(*inputQueue, &holder, ( TickType_t ) 10) == pdPASS) {
            Input_Receive_listener(holder);
        }
        vTaskDelay(100 / portTICK_RATE_MS); 
    }
}
