#include "Input_Queue.h"

InputQueueHandle inputQueue = NULL;

InputQueueHandle *Input_GetInputQueueHandle(void) {
    return &inputQueue;
}

InputQueueHandle *Input_Queue_Init(void) {
    if (inputQueue == NULL)
    {
        inputQueue = xQueueCreate(INPUT_QUEUE_SIZE, sizeof(int));
    }

    return &inputQueue;
}

void Input_ResetInputQueue(void) {
    xQueueReset(inputQueue);
}