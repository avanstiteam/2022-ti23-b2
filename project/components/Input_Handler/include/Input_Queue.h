#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#pragma once

#define INPUT_QUEUE_SIZE 30

typedef QueueHandle_t InputQueueHandle;

InputQueueHandle *Input_GetInputQueueHandle(void);
InputQueueHandle *Input_Queue_Init(void);
void Input_ResetInputQueue(void);