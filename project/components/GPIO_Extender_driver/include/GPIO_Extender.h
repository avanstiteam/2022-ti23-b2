#ifndef GPIO_EXTENDER
#define GPIO_EXTENDER

#include "mcp23017.h"

extern mcp23017_t mcp23017;

// Define the adress of the GPIO extender
#define GPIO_EXTENDER_ADDRESS 0x20


// Inits the communication to the GPIO extender
// PORTA is set to output
// PORTB is set to input
void GPIO_Extender_init();

// Given the register, group and value, that byte is writen to the extender
mcp23017_err_t GPIO_Extender_write_register(mcp23017_reg_t reg, mcp23017_gpio_t group, uint8_t v);

// Given de register, group and pointer to result, byte is returned in data
mcp23017_err_t GPIO_Extender_read_register(mcp23017_reg_t reg, mcp23017_gpio_t group, uint8_t *data);

#endif