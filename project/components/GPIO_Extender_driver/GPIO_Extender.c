#include "GPIO_Extender.h"

mcp23017_t mcp23017;


// Inits the communication to the GPIO extender
// PORTA is set to outpuy
// PORTB is set to input
void GPIO_Extender_init() {
     // Initialize I2C bus
	mcp23017.i2c_addr = GPIO_EXTENDER_ADDRESS;
	mcp23017.sda_pin = 18;
	mcp23017.scl_pin = 23;
	mcp23017.sda_pullup_en = GPIO_PULLUP_ENABLE;
	mcp23017.scl_pullup_en = GPIO_PULLUP_ENABLE;
	
	// Set GPIO Direction
	mcp23017_write_register(&mcp23017, MCP23017_IODIR, GPIOA, 0x00); // full port on OUTPUT
	mcp23017_write_register(&mcp23017, MCP23017_IODIR, GPIOB, 0x00); // full port on OUTPUT
	//mcp23017_write_register(&mcp23017, MCP23017_GPPU, GPIOB,  0x01); // Enable pullup on B0s
}

// Given the register, group and value, that byte is writen to the extender
mcp23017_err_t GPIO_Extender_write_register(mcp23017_reg_t reg, mcp23017_gpio_t group, uint8_t v) {
    return mcp23017_write_register(&mcp23017, reg, group, v);
}

// Given de register, group and pointer to result, byte is returned in data
mcp23017_err_t GPIO_Extender_read_register(mcp23017_reg_t reg, mcp23017_gpio_t group, uint8_t *data) {
    return mcp23017_read_register(&mcp23017, reg, group, data);
}