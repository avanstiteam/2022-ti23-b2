/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include "LCDDriver.h"
#include "menu_strings.h"
#include "Input_Codes.h"
#include "PartyBoxMenu.h"
#include "AbstractMenu.h"
#include "Input_Receive.h"
#include "Input_Send.h"
#include "Input_Receive.h"
#include "Input_Codes.h"
#include "PartyBoxMenu.h"
#include "Rotary_encoder_driver.h"
#include "Buttons_controller.h"
#include "custom_icon.h"
#include "wifi.h"
#include "GPIO_Extender.h"
#include "mcp23017.h"
#include "SSEG_Driver.h"
#include "Audio_Handler.h"
#include "audio_recognition.h"

#include "time_manager.h"
#include "GKAR_General_Karaoke.h"
#include "LGM_module.h"

void show_boot_screen(void *pvParameters);
void wait_for_wifi_connection(void);
void runMenuTest();
void Input_Receive(int menu_key);
void onTimeCall(time_t t);

#include "system_time.h"
#include "SPI_SSEG_Driver.h"
#include <time.h>
#include <sys/time.h>
#include "Volume_Receive.h"
#include "Kar_Time.h"

#include "GKAR_General_Karaoke.h"

static const char *TAG = "MAIN";

void app_main(void)
{
    esp_log_level_set("*", ESP_LOG_WARN);
    esp_log_level_set(TAG, ESP_LOG_INFO);

    Audio_Init();
    LCD_setup();
    Kar_Time_init();
    setup_Icons();

    
    GPIO_Extender_init();

    TaskHandle_t xHandle = NULL;
    xTaskCreate(init_wifi, "init_wifi", 4096, NULL, 1, NULL);
    xTaskCreate(show_boot_screen, "boot_screen", 2048, NULL, 1, &xHandle);

    //xTaskCreate(test_tasks, "test_task", 2048, NULL, 1, NULL);

    wait_for_wifi_connection();

    if (get_wifi_state() == WIFI_NOT_CONNECTED)
    {
        vTaskDelete(xHandle);
        xHandle = NULL;
        LCD_Clear_Screen();
        LCD_Write_String_Middle("Wifi failed", 1);
        LCD_Write_String_Middle("Please check", 2);
        LCD_Write_String_Middle("and restart", 3);
        // Blocking call until user restarts the device.
        for (;;)
        {
            wait_for_wifi_connection();
            if (get_wifi_state() != WIFI_NOT_CONNECTED)
            {
                break;
            }
        }
    }
    
    ESP_LOGI("WIFI STATE", "%d", get_wifi_state());

    BUTTONS_init(); // setup buttons

    // // Starting the sntp (time protocol)
    TIME_start_sntp();

    // Starting the time updates to the SSEG
    SPI_SSEG_init();
    TIME_start_SSEG_time();

    if (xHandle != NULL)
    {
        vTaskDelete(xHandle);
    }
    LCD_Clear_Screen();

    // // Start the menu to the boot screen
    abstract_menu_startMenu(&firstMenuScreen);
    
    GKAR_Init();

    AUDIO_DETECTION_Start();

    while (1)
    {
        vTaskDelay(4000 / portTICK_PERIOD_MS);
    }
}

void wait_for_wifi_connection(void)
{
    for (size_t i = 0; i < 30; i++)
    {
        if (get_wifi_state() == 0)
        {
            return;
        }
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}


// void test_tasks(void *pvParameters)
// {
//     int millis;
//     for(;;)
//     {
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//         ESP_LOGI(TAG, "Kar_Time_get_millis");
//         millis = Kar_Time_get_millis();
//         ESP_LOGI(TAG, "Kar_Time_got_millis");
//         ESP_LOGI(TAG, "new millis %d", millis);
//     }
// }

void show_boot_screen(void *pvParameters)
{
    LCD_Write_String_Middle(BOOT_HEADER_1, 1);
    LCD_Write_String_Middle(BOOT_HEADER_2, 2);
    LCD_Write_String_At(BOOT_HEADER_3, 3, 0);

    for (;;)
    {
        for (size_t i = 0; i < 20; i++)
        {
            LCD_Write_String_At("* ", 3, i);
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
    }
}