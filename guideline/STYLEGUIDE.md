# Code style guide

## Code structure

- No double empty lines!
- Last lines of file always has to be empty for git.
- Pascal case for variables.
- Functions will use camel snake case.
- Variables will use pascal case.
- constants should always be fully uppercase
- Always braces even if it is one line.
- No unnecesary abreviations.
- If p is a pointer to a structure, you should access members by using p->member rather than (*p).member.
- If p is a pointer to a structure, do not surround the -> operator with spaces (use p->id instead of p -> id).
- includes will always be at the top of the file.
- defines should always be placed at the top of the file underneath the includes.

## if statements

### In general check if you can avoid an else statement.

```c
if (provider == null) { provider = CultureInfo.CurrentCulture };

if (statement) {
    // Code here
}

if (statement) {
    // Code here
} 
else {
    // Other code here
}

if (statement) {
    // Code here
} 
else if (statement) {
    // Other code here
}
```

## Switch statements

```c

switch (variable or an integer expression) {
    case constant:
    //C Statements
    ;
    case constant:
    //C Statements
    ;
    default:
    //C Statements
    ;
}
```

## Methodes
### Methodes should always do one task if it needs to do more split it up in multiple methodes.
### If the methode comes from a header file put in front of the methode the abreviation of the module in caps with a max of 4 letters.
### When you use a bit code for an instruction use a comment to explain what this instruction will do.
```c

/*
 * Explain here what the method does
 * Explain why a method is used
 * Variabels: (Type, name),(Type,Name)
 * Returns: Say what is returns
 */
void MOD_Method(Data data) {
    // Code here
}

```

## Structs
### Every created struct should use the typdef function as in the example below.
### This is to improve readability

```c

typedef struct {
        char title;
        int pageCount;
        int publishYear;
    } Book;

```

## Headers

- Do not include *.c files only *.h or *.a files should be included.
- If after a chage an import becomes obsolete remove it from the *.c file.
- A method should only be in the header file if is can be used outside off the *.c file.


## Comments

- All methods should be commented, as by the example in [Methodes](#Methodes).
- Comments that explain an instruction or byte should be after the text.