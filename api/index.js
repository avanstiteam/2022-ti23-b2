const express = require('express');
const fs = require("fs");
const app = express();
const port = 8082;

const fileExists = async path => !!(await fs.promises.stat(path).catch(e => false));

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({extended: true})); // for parsing application/x-www-form-urlencodeds

app.get('/getlyrics', async (req, res) => {
    const song = req.query.song.toString();

    if (song.indexOf('..') !== -1 || song.indexOf('/') !== -1) {
        return res.sendStatus(401);
    }
    const songPath = './LRC files/' + song + '.lrc';

    if (!(await fileExists(songPath))) {
        console.log("no file found");
        return res.sendStatus(401);
    }

    fs.readFile(songPath, 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            return;
        }

        return res.send(data);
    });
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});


